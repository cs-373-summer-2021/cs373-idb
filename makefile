
IDB3.log: IDB3.log
	git log > IDB3.log

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

# DOCKER - Build and Deploy Environment (Backend/Frontend/Database)
docker-build:
	docker-compose build

docker-deploy:
	docker-compose up -d

docker-start:
	docker-compose start

docker-stop:
	docker-compose stop

docker-watch:
	docker-compose ps

#TESTS
python-unit-tests:
	cd backend && \
	$(PYTHON) tests.py

postman-tests:
	newman run BookSleuth_Testing.postman_collection.json

tests.tmp:
	cd backend && \
	$(COVERAGE) run    --branch tests.py >  tests.tmp 2>&1 && \
	$(COVERAGE) report -m                      >> tests.tmp && \
	cat tests.tmp

postman-tests:
	newman run postman_tests.json

#MISC
clean:
	cd backend && \
	rm -f  .coverage  && \
	rm -f  *.pyc  && \
	rm -f  tests.tmp  && \
	rm -rf __pycache__

models.html:
	cd backend  && \
	$(PYDOC) -w models

config:
	git config -l

format:
	$(AUTOPEP8) -i main.py
	$(AUTOPEP8) -i models.py
	$(AUTOPEP8) -i tests.py

versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version
	
test: models.html IDB3.log tests.tmp
