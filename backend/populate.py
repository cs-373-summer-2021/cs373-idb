from models import app, db, Author, Book, Publisher, Subjects 
from scraper import publisher_parser, editionstoDictList, searchPublisher, authorOLID



def fill_publishers():
    image_links = open('PublisherImageLinks.txt', 'r')
    with open('PublisherNames.txt', 'r') as inp:
        for publisher in inp:
            image_link = image_links.readline().rstrip('\n')
            publisher = publisher.strip()
            dic = publisher_parser(publisher)
            if dic.get('name') == None:
                dic['name'] = 'N/A'
            if dic.get('status') == None:
                dic['status'] = 'inactive'
            if dic.get('website') == None:
                dic['website'] = 'N/A'
            if dic.get('country') == None:
                dic['country'] = 'N/A'
            if dic.get('founded') == None:
                dic['founded'] = 'N/A'
            if dic.get('headquarters') == None:
                dic['headquarters'] = 'N/A'
            db_publisher = Publisher(name = dic['name'], status = dic['status'], website = dic['website'], country = dic['country'], founded = dic['founded'], headquarters = dic['headquarters'], image_link=image_link)
            db.session.add(db_publisher)
    db.session.commit()

def fill_books():
    dicList = editionstoDictList()
    dump_keys = ['number_of_pages', 'publishers', 'title', 'subtitle', 'description', 'isbn_13', 'isbn_10', 'key', 'authors', 'subjects', 'works', 'publish_date']
    for dic in dicList:
        publishers = dic.get('publishers')
        publisher_id = 0
        publisher_object = Publisher.query.filter_by(id = 0).first()
        if publishers != None:
            publisher = publishers[0]
            publisher_name = searchPublisher(publisher)
            if publisher_name == None:
                continue
            publisher_name = publisher_name.replace('_', ' ')
            temp = Publisher.query.filter_by(name = publisher_name).first()
            if temp == None:
                continue
            # print("succeeded")
            # print(temp.id)
            publisher_object = temp
            publisher_id = publisher_object.id
        else:
            continue
        num_pages = dic.get('number_of_pages')
        if num_pages == None:
            num_pages = 0
        title = dic.get('title')
        if title == None:
            title = 'N/A'
        #subtitle is the only nullable attribute
        subtitle = dic.get('subtitle')
        sub_dict = dic.get('description')
        description = "N/A"
        if sub_dict != None:
            #sometimes description will return a string not a dict
            if type(sub_dict) is dict:
                description = sub_dict['value']
            else:
                description = sub_dict
        isbn = "N/A"
        temp = dic.get("isbn_13")
        if temp != None:
            isbn = temp
        else:
            temp = dic.get('isbn_10')
            if temp != None:
                isbn = temp
        OLID = dic.get('key')
        authors = dic.get('authors')
        author_OLIDs = []
        author_Objects = []
        if authors != None:
            for author in authors:
                # author OLIDs are returned in the form "/authors/OLXXXXA"
                author_OLIDs.append(author['key'])
            
            for auth_id in author_OLIDs:
                #come back to
                temp = insert_author(auth_id)
                if temp == None:
                    continue
                author_Objects.append(temp)
            db.session.commit()
        else:
            continue
        publish_date = dic.get("publish_date")
        if publish_date == None:
            publish_date = 'N/A'
        subjects = dic.get("subjects")
        subjectObjects = []
        if subjects == None:
            subjects = ["N/A"]
        for subject in subjects:
            subjectObjects.append(insert_subject(subject))
        sub_dict = dic.get("works")
        work_OLID = "N/A"
        if sub_dict != None:
            temp = sub_dict[0].get("key")
            if temp != None:
                work_OLID = temp
        
        db_book = Book(title = title, OLID = OLID, work_id = work_OLID, subtitle = subtitle, page_count = num_pages, description=description, isbn = isbn, publish_date=publish_date, publisher_id = publisher_id)
        db.session.add(db_book)
        db.session.flush()
        for author in author_Objects:
            if author != None:
                db_book.authors.append(author)
                publisher_object.authors.append(author)
        for subject in subjectObjects:
            db_book.subjects.append(subject)
        db.session.commit()


        
        
            

def insert_author(author_OLID):
    author_object = Author.query.filter_by(OLID = author_OLID).first()
    if(author_object == None):
        author_dict = authorOLID(author_OLID)
        print(author_dict)
        if(author_dict == None):
            return None
        # else:
        #     author_dict = authorOLID(author_OLID)['docs'][0] 
        OLID = author_OLID
        name = author_dict.get("name")
        if name == None:
            name = "N/A"
        birth_date = author_dict.get("birth_date")
        death_date = author_dict.get("death_date")
        if birth_date == None:
            birth_date = "N/A"
        if death_date == None:
            death_date = "N/A"
        sub_dict = author_dict.get("bio")
        bio = "N/A"
        if sub_dict != None:
            #sometimes description will return a string not a dict
            if type(sub_dict) is dict:
                bio = sub_dict['value']
            else:
                bio = sub_dict
        temp = author_dict.get("top_work")
        top_work = "N/A"
        if temp != None:
            top_work = temp
        #figure out links later
        website = 'N/A'
        temp = author_dict.get('links')
        if temp != None:
            website = temp[0].get('url')
        else:
            temp = author_dict.get('wikipedia')
            if temp != None:
                website = temp
        author_object = Author(name = name, OLID = OLID, bio = bio, birth_date = birth_date, death_date = death_date, top_work = top_work, website_link = website)
        db.session.add(author_object)
        db.session.commit()
    return author_object

        






def insert_subject(subject_text):
    subjectObject = Subjects.query.filter_by(name = subject_text).first()
    if subjectObject == None:
        subjectObject = Subjects(name = subject_text)
        db.session.add(subjectObject)
    return subjectObject


if __name__ == '__main__':
    print("Entered")
    db.drop_all()
    print("Dropped_all")
    db.create_all()
    print("Created_all")
    defaultPublisher = Publisher(id = 0, name = "N/A", status = "Inactive", website = "N/A", country = "N/A", headquarters = "N/A", founded = "N/A", image_link = 'N/A')
    db.session.add(defaultPublisher)
    db.session.commit()
    fill_publishers()
    fill_books()
    print("Done")
    