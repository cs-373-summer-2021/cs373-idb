import requests
import json
import re
import unicodedata

# isbns = [9780486280615, 1503290565, 9789354860591]
# google_books_url = "https://www.googleapis.com/books/v1/volumes"
# books_columns = ["id", "title", "subtitle", "authors", "publisher", "publishDate", 
# "description", "pageCount", "categories", "averageRating", "ratingsCount", "maturityRating", 
# "language", "imageLinks"]

# def get_works():
#     print()
#     for isbn in isbns:
#         parameters = {'q': isbn}
#         results = requests.get(google_books_url, params=parameters)
#         if results.status_code == 200:
#             r = results.json()
#             first_item = r["items"][0]
#             print(first_item)
#             print()
#             print()



def only_numerics(seq):
    seq_type= type(seq)
    return seq_type().join(filter(seq_type.isdigit, seq))





#NOTE: requests.get might not work if you're using a VPN
#Assumes title is an exact match for the article
#return:
def query_wikimedia(title):
    rvsection = '0'
    parameters = {'action':'query', 'prop' :  'revisions', 'rvprop' : 'content', 'rvsection' : rvsection, 'rvslots' : 'main', 'format' : 'json', 'titles' : title}
    wikimedia_url = "https://en.wikipedia.org/w/api.php"
    r = requests.get(wikimedia_url, params = parameters)
    #TODO: add error handling

    #Uncomment if you just want the text representation of the JSON
    #print(r.text)
    # print(r.url)
    #Note: Need to figure out how to only store the revisions dictionary
    r_dict = json.loads(r.text)
    page_id = (list(r_dict['query']['pages'].keys())[0])
    #data_str is the string that composes the corresponding article 
    # print(list(r_dict), title)
    # print(page_id)
    data_str = r_dict['query']['pages'][page_id]['revisions'][int(rvsection)]['slots']["main"]["*"]
    return data_str

# def query_open(id) {
#     https://openlibrary.org/api/books?bibkeys=ISBN:0451526538&callback=mycallback
# }


#Takes in a writer's wikimedia article string, returns a dictionary with relevant data extracted 
def parse_writer_str(data_str):
    infobox_start = data_str.find("Infobox")
    data_str = data_str[infobox_start:]
    str_lines = data_str.split('\n')
    # for v in str_lines:
    #     print(v)
    #     print()
    i = 1
    size = 0
    writer_dict = {}
    #fill up dict with infobox key value
    while(True):
        line = str_lines[i]
        size += len(line)
        line = line[2:]
        first_eq = line.find('=')
        key = line[:first_eq - 1]
        val = line[first_eq + 2:]
        if(key == ""):
            break
        if(val != ""):
            writer_dict.update({key: val})
        i += 1
    description = ""
    
    for line in range(i + 1, len(str_lines)):
        description += str_lines[line] + '\n'
    writer_dict.update({"description": description})
    return writer_dict

def publisher_parser(title):
    data_str = query_wikimedia(title)
    # print(data_str)
    expected_keys = ['name', 'status', 'website', 'country', 'headquarters', 'founded']
    output = {}
    for key in expected_keys:
        output[key] = 'N/A'
    temp = title.replace("_", " ")
    output['name'] = temp
    output['status'] = 'Inactive'
    infobox_start = data_str.find("Infobox")
    data_str = data_str[infobox_start:]
    str_lines = data_str.split('\n')
    for i in str_lines:
        if i.find('imprint'):
            output['status'] = 'Active'
        if i.find('URL') != -1:
            for i2 in i.split('|'):
                 if i2.find('.') != -1:
                     output['website'] = i2.replace("}", "")
        if i.find('founded') != -1:
            for i2 in i.split('|'):
                if len(only_numerics(i2)) == 4:
                    output['founded'] = only_numerics(i2)
        if i.find('country') != -1:
            list1 = i.split('=')
            input = list1[1][1:]
            s = input.replace("}","")
            s = s.replace("{","")
            s = s.replace("[","")
            s = s.replace("]","")
            s = s.replace("br"," and ")
            s = s.replace("<","")
            s = s.replace(">","")
            output['country'] = s
        if i.find('status') != -1:
            for i2 in i.split("="):
                if i2.find('Active') == 1:
                    output['status'] = 'Active'
    changed = 0
    changed2 = 0
    for i in str_lines:
        if i.find('| url') != -1:
            if i.find('</ref>') == -1:
                i2 = (i.split('='))
                for i3 in i2:
                    if i3.find('http') != -1:
                        for i4 in i3.split(' '):
                            if i4.find('http') != -1:
                                if ('website' in output) == False:
                                    if changed2 == 0:
                                        temp = i4
                                        s = temp.replace("}","")
                                        s = s.replace("{","")
                                        s = s.replace("[","")
                                        s = s.replace("]","")
                                        output['website'] = s
                                        changed = 1
        if i.find('| headquarters') != -1:
            base = i[len('| headquarters = '):]
            res = ''
            # print(base)
            hasDubs = True
            while hasDubs:
                dub1 = i.find('[[')
                dub2 = i.find(']]')
                if dub1 != -1 and dub2 != -1:
                    temp = i[dub1 + 2:dub2]
                    if temp.find('|') != -1:
                        temp = temp[temp.rfind('|') + 1: dub2]
                    res = res + temp + ', '
                    i = i[dub1 + 2: dub2]
                else:
                    hasDubs = False
            else:
                if(res.rfind(', ') != -1):
                    res = res[:res.rfind(', ')]
                if(res == ''):
                    res = base
            if(res != ''):
                output['headquarters'] = res
            # print(output.get('headquarters'))
        if i.find('|headquarters') != -1:
            base = i[len('|headquarters = '):]
            res = ''
            # print(base)
            hasDubs = True
            while hasDubs:
                dub1 = i.find('[[')
                dub2 = i.find(']]')
                if dub1 != -1 and dub2 != -1:
                    temp = i[dub1 + 2:dub2]
                    if temp.find('|') != -1:
                        temp = temp[temp.rfind('|') + 1: dub2]
                    res = res + temp + ', '
                    i = i[dub1 + 2: dub2]
                else:
                    hasDubs = False
            else:
                if(res.rfind(', ') != -1):
                    res = res[:res.rfind(', ')]
                if(res == ''):
                    res = base
            if(res != ''):
                output['headquarters'] = res

    return output

def author_parser(data_str):
    output = {}
    date = ""
    birthplace = ""
    notableworks = ""
    infobox_start = data_str.find("Infobox")
    data_str = data_str[infobox_start:]
    str_lines = data_str.split('\n')
    for i in str_lines:
        if i.find('birth_date') != -1:
            parsed = i.replace("}", "")
            parsed = parsed.replace("{","")
            for i2 in parsed.split("|"):
                if(len(only_numerics(i2))!=0):
                    date += i2
                    date += "|"
        if i.find('birth_place') != -1:
            for i2 in i.split(" "):
                if i2.find("]") != -1:
                    parsed = i2.replace("]", "")
                    parsed = parsed.replace("[", "")
                    birthplace += parsed
                    birthplace += " "
        if i.find('nationality') != -1:
            output['Nationality'] = (i.split(" ")[-1])
        if i.find('notableworks') != -1:
            for i2 in i.split("''"):
                if i2.find('[[') != -1:
                    parsed = i2.replace("[[","")
                    parsed = parsed.replace("]]","")
                    notableworks += parsed
                    notableworks += ', '

    date = date[:-1]
    birthplace = birthplace[:-2]
    notableworks = notableworks[:-2]
    output['Place of Birth'] = birthplace
    output['Birth_Date'] = date
    output['Notable Works'] = notableworks
    print(output)

def extractTitles():
    with open('Dump.txt', 'r') as dump:
        with open('titles.txt', 'w') as out:
            for line in dump:
                i = line.find("\"title\": ")
                i = i + 7
                start = line.find("\"", i)
                end = line.find("\",", start + 1)
                res = line[start + 1:end]
                if(res.find('/') == -1):
                    out.write(res + "\n")

def extractOLID():
    with open('Dump.txt', 'r') as dump:
        with open('ids.txt', 'w') as out:
            for line in dump:
                i = line.find("/works/")
                e = line.find(" ", i + 1)
                res = line[i:e]
                out.write(res + "\n")


def editionsScrape():
    with open('Dump.txt', 'r') as dump:
        with open('books.txt', 'w') as out:
            for line in dump:
                i = line.find("{")
                out.write(line[i:])

            
def editionstoDictList():
    dictList = []
    with open('books.txt', 'r') as input:
        for line in input:
            bookDict = json.loads(line)
            dictList.append(bookDict)
    print("FINISHED DICT LIST")
    return dictList

def searchPublisher(raw):
    temp = raw.lower().strip()
    with open('PublisherNames.txt', 'r') as publishers:
        for publisher in publishers:
            s = str(publisher.lower())
            s = str(s.strip())
            if(s.find(temp) != -1):
                return publisher.strip()
        #No match found output phrase that missed to file
    with open("publisherMisses.txt", 'a') as out:
        out.write(raw + '\n')
    return None

def authorOLID(auth_id):
    # auth_id = auth_id.strip()
    # auth_id = auth_id[auth_id.find("OL"):]
    # OL_URL = "https://openlibrary.org/search/authors.json?q=" + auth_id
    # OL_URL = "https://openlibrary.org/" + auth_id + ".json"
    # r = requests.get(OL_URL)
    # rtext = r.text
    # print(r.status_code)
    # print(r.text)
    rtext = ''
    # print("OLID",auth_id)
    with open('matches.txt', 'r') as authors:
        for author in authors:
            # print(author)
            found = author.find(auth_id)

            if found != -1:
                found = author.find('{')
                rtext = unicodedata.normalize('NFC', author[found:])
                try:
                    print("In try block")
                    # print(rtext)
                    # print(json.loads(rtext))
                    return json.loads(rtext)

                except:
                    return None 
    return None
                




if __name__ == '__main__':
    pass
    # print(publisher_parser('Random_House'))
    # with open('PublisherNames.txt', 'r') as publishers:
    #     for pub in publishers:
    #         pub = pub.strip()
    #         dic = publisher_parser(pub)
    #         # print(dic.get('headquarters'))
    #         if(dic.get('headquarters') == 'N/A'):
    #             print(dic.get('name'))
    #         if dic.get('name') == None:
    #             dic['name'] = 'N/A'
    #         if dic.get('status') == None:
    #             dic['status'] = 'inactive'
    #         if dic.get('website') == None:
    #             dic['website'] = 'N/A'
    #         if dic.get('country') == None:
    #             dic['country'] = 'N/A'
    #         if dic.get('founded') == None:
    #             dic['founded'] = 'N/A'
    #         if dic.get('headquarters') == None:
    #             dic['headquarters'] = 'N/A'

    # get_works()
    # print(query_wikimedia("Hachette_Book_Group"))
    # print(query_wikimedia("Academic_Press"))
    # print(query_wikimedia("Hachette_Book_Group").split('|'))
    # print(publisher_parser("Penguin_Books"))
   # print(publisher_parser(query_wikimedia("Alma_Books")))
    #extractTitles()
    #extractOLID()
   # print(publisher_parser("Academic_Press"))
    #print(publisher_parser("Ballantine_Books"))
    # extractTitles()
    # editionsScrape()
    # dictList = editionstoDictList()
    # random = dictList[56]
    # print(str(random["publishers"]))
    # print(random["subjects"])
    # print(random["authors"])
    # # with open("temp.txt", 'w') as out:
    #     for line in dictList:
    #         out.write(str(line) + '\n')
                        
    # print(publisher_parser("Hachette_Book_Group"))
    # print(publisher_parser("Academic_Press"))
    # print(publisher_parser("Ballantine_Books"))
    #print(publisher_parser(query_wikimedia("Academic_Press")))
    #print(publisher_parser(query_wikimedia("Macmillan_Publishers")))
    #print(publisher_parser(query_wikimedia("Ballantine_Books")))
    # print(query_wikimedia("Macmillan_Publishers"))
    #print(query_wikimedia("George_Orwell"))
    #data_str = parse_writer_str(data_str)
    #data_str = parse_writer_str(query_wikimedia("Penguin_Books"))
    #print(query_wikimedia("Mark_Twain"))
    #author_parser(query_wikimedia("George_Orwell"))
    #print(data_str['birth_date'].split('|'))
    #with open("temp.json", "w") as outfile:
    #    json.dump(parse_writer_str(data_str), fp = outfile)

    #query_wikimedia("Edgar_Allan_Poe")
    #query_wikimedia("Arthur_Conan_Doyle")
