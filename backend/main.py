import sys
from flask import Flask, render_template, url_for, jsonify, request
from sqlalchemy import or_, nullslast
from flask_cors import CORS
from sqlalchemy.sql.elements import Null
from search import search_author, search_publisher, search_work
from models import app, db, Author, Book, Publisher, Subjects, AuthorSchema, BookSchema, PublisherSchema
import json
import unittest
import tests
import search

# app = Flask(__name__)
# cors = CORS(app)

author_schema = AuthorSchema()
publisher_schema = PublisherSchema()
book_schema = BookSchema()

# ----- TEMPORARY DEFAULT PATH - Visual API ------


@app.route('/')
def temp():
    return render_template("temp.html")

# END TEMP API

# Api Template
# GET all publishers sorted


@app.route('/api/publisher/all/sorted', methods=["GET"])
def getPublishersSorted():
    # result_json = publisher_schema.dump(result, many=True)
    args = request.args.to_dict(flat=True)
    # result_list = db.session.query(Author).all()
    result_list = db.session.query(Publisher)
    ordering = args.get('ordering')
    orderBy = args.get('orderBy')
    status = args.get('status')
    # expected 'founded' format: '...&founded_range=1945-2002' assumes - is included and params are years
    founded = args.get('founded_range')
    min_founded = '0'
    max_founded = '9'
    country = args.get('country')
    search = args.get('q')
    if search is not None:
        search = search.strip()
    if search != None and search != '':
        result_list = search_publisher(result_list, search)
    if ordering == None or ordering == '':
        ordering = 'asc'
    if founded != None and founded != '':
        temp = founded.split('-')
        #  Only takes first two values, if input is garbage defaults to all
        if temp[0].isdecimal():
            min_founded = temp[0]
        if temp[1].isdecimal():
            max_founded = temp[1]
        if min_founded > max_founded:
            swap = max_founded
            max_founded = min_founded
            min_founded = swap
        result_list = result_list.filter(Publisher.founded != 'N/A')
        result_list = result_list.filter(
            Publisher.founded > min_founded, Publisher.founded < max_founded)
    if country != None and country != '':
        country = country.lower().strip()
        result_list = result_list.filter(
            Publisher.country.ilike('%' + country + '%'))
    if status != None and status != '':
        result_list = result_list.filter(Publisher.status == status)
    if orderBy != None and orderBy != '':
        if orderBy == 'name':
            result_list = result_list.filter(Publisher.name != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.name)
            else:
                result_list = result_list.order_by(Publisher.name.desc())
        elif orderBy == 'status':
            result_list = result_list.filter(Publisher.status != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.status)
            else:
                result_list = result_list.order_by(Publisher.status.desc())
        elif orderBy == 'website':
            result_list = result_list.filter(Publisher.website != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.website)
            else:
                result_list = result_list.order_by(Publisher.website.desc())
        elif orderBy == 'country':
            result_list = result_list.filter(Publisher.country != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.country)
            else:
                result_list = result_list.order_by(Publisher.country.desc())
        elif orderBy == 'headquarters':
            result_list = result_list.filter(Publisher.headquarters != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.headquarters)
            else:
                result_list = result_list.order_by(
                    Publisher.headquarters.desc())
        elif orderBy == 'founded':
            result_list = result_list.filter(Publisher.founded != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Publisher.founded)
            else:
                result_list = result_list.order_by(Publisher.founded.desc())
    else:
        if ordering == 'desc':
            result_list = result_list.order_by(Publisher.id.desc())

    # Pagination
    per_page = args.get('per_page')
    page = args.get('page')
    if per_page is None:
        per_page = 20
    if page is None:
        page = 1
    count = result_list.count()
    result_page = result_list.paginate(page=int(page), per_page=int(per_page))
    result = publisher_schema.dump(result_page.items, many=True)
    return jsonify({'count': count, 'results': result})


# GET all publishers
@app.route('/api/publisher/all')
def getPublishers():
    # result_json = publisher_schema.dump(result, many=True)
    result_list = db.session.query(Publisher).all()
    result = []
    for elem in result_list:
        if elem.id != 0:
            result.append(publisher_schema.dump(elem))
    return jsonify(result)


# GET publisher given id
@app.route('/api/publisher/<int:id>')
def getPublisher(id):
    result = db.session.query(Publisher).filter_by(id=id).first()
    return jsonify(publisher_schema.dump(result))

# GET all works sorted


@app.route('/api/work/all/sorted', methods=["GET"])
def getWorksSorted():
    # result_json = publisher_schema.dump(result, many=True)
    args = request.args.to_dict(flat=True)
    # result_list = db.session.query(Author).all()
    result_list = db.session.query(Book)
    ordering = args.get('ordering')
    orderBy = args.get('orderBy')
    if ordering == None:
        ordering = 'asc'
    # Page Count format = '...&page_range=120-240', assumes '-' is included
    page_range_str = args.get('page_range')
    page_min = 0
    page_max = sys.maxsize
    # Publish Date format = '...&published_range=1849-2002' assumes '-' is included
    publish_range_str = args.get('published_range')
    publish_date_min = '0'
    publish_date_max = '9'
    # Treats subject as a query string against the subjects related to the book
    subject_str = args.get('subject')
    # Get All Titles that start with 'starts_with'
    #  Expected format '...&starts_with=a, can be any alphanumeric/s
    start_with = args.get('starts_with')
    if start_with is not None:
        start_with.strip()
    if start_with != None and start_with != '':
        start_with = start_with + '%'
        result_list = result_list.filter(Book.title.ilike(start_with))
    search = args.get('q')
    if search is not None:
        search.strip()
    if search != None and search != '':
        result_list = search_work(result_list, search)

    if page_range_str != None and page_range_str != '':
        temp = page_range_str.split('-')
        # Only update page min and max if input is a number
        if temp[0].isdecimal():
            page_min = int(temp[0])
        if temp[1].isdecimal():
            page_max = int(temp[1])
        # if page_max < page_min:
        #     # Handle reversed bounds
        #     swap = page_max
        #     page_max = page_min
        #     page_min = swap
        result_list = result_list.filter(
            page_min <= Book.page_count, page_max >= Book.page_count)

    if publish_range_str != None and publish_range_str != '':
        temp = publish_range_str.split('-')
        if temp[0].isdecimal():
            publish_date_min = temp[0]
        if temp[1].isdecimal():
            publish_date_max = temp[1]
        # if publish_date_min < publish_date_max:
        #     swap = publish_date_max
        #     publish_date_max = publish_date_min
        #     publish_date_min = swap
        result_list = result_list.filter(Book.publish_date != 'N/A')
        result_list = result_list.filter(
            Book.publish_date >= publish_date_min, Book.publish_date <= publish_date_max)

    if subject_str != None and subject_str != '':
        subject_str = subject_str.lower().strip()
        result_list = result_list.filter(
            Publisher.subjects.any(Subjects.name.contains(subject_str)))

    if orderBy != None and orderBy != '':
        if orderBy == 'OLID':
            result_list = result_list.filter(Book.OLID != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.OLID)
            else:
                result_list = result_list.order_by(Book.OLID.desc())
        elif orderBy == 'work_id':
            result_list = result_list.filter(Book.work_id != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.work_id)
            else:
                result_list = result_list.order_by(Book.work_id.desc())
        elif orderBy == 'title':
            result_list = result_list.filter(Book.title != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.title)
            else:
                result_list = result_list.order_by(Book.work_id.desc())
        elif orderBy == 'subtitle':
            result_list = result_list.filter(Book.subtitle != Null, Book.subtitle != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.subtitle)
            else:
                result_list = result_list.order_by(Book.subtitle.desc())
        elif orderBy == 'page_count':
            result_list = result_list.filter()
            if ordering == 'asc':
                result_list = result_list.order_by(Book.page_count)
            else:
                result_list = result_list.order_by(Book.page_count.desc())
        elif orderBy == 'description':
            result_list = result_list.filter(Book.description != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.description)
            else:
                result_list = result_list.order_by(Book.description.desc())
        elif orderBy == 'isbn':
            result_list = result_list.filter(Book.isbn != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.isbn)
            else:
                result_list = result_list.order_by(Book.isbn.desc())
        elif orderBy == 'publish_date':
            result_list = result_list.filter(Book.publish_date != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Book.publish_date)
            else:
                result_list = result_list.order_by(Book.publish_date.desc())
    else:
        if ordering == 'desc':
            result_list = result_list.order_by(Book.id.desc())

    # Pagination
    per_page = args.get('per_page')
    page = args.get('page')
    if per_page is None:
        per_page = 20
    if page is None:
        page = 1
    count = result_list.count()
    result_page = result_list.paginate(page=int(page), per_page=int(per_page))
    result = book_schema.dump(result_page.items, many=True)
    return jsonify({'count': count, 'results': result})


# GET all works
@ app.route('/api/author/all')
def getAuthors():
    result_list = db.session.query(Author).all()
    result = []
    for elem in result_list:
        result.append(author_schema.dump(elem))
    # result_json = author_schema.dump(result, many=True)
    return jsonify(result)

# GET all authors SORTED


@ app.route('/api/author/all/sorted', methods=["GET"])
def getAuthorsSorted():
    args = request.args.to_dict(flat=True)
    # result_list = db.session.query(Author).all()
    result_list = db.session.query(Author)
    ordering = args.get('ordering')
    orderBy = args.get('orderBy')
    birthday_min = None
    birthday_max = None
    # Expected format: '...&birthday_range=1942-2011' where the values are only years, assumes - is included
    birthday_str = args.get('birthday_range')
    if birthday_str != None and birthday_str != '':
        temp = birthday_str.split('-')
        if temp[0].isdecimal():
            birthday_min = temp[0]
        if len(temp) > 1 and temp[1].isdecimal():
            birthday_max = temp[1]
        result_list = result_list.filter(Author.birth_date != 'N/A')
        result_list = result_list.filter(
            Author.birth_date >= birthday_min, Author.birth_date <= birthday_max)
    # Expected format: '...&deathday_range=1942-2011' where the values are only years, assumes - is included
    deathday_str = args.get('deathday_range')
    if deathday_str != None and deathday_str != '':
        temp = deathday_str.split('-')
        if temp[0].isdecimal():
            deathday_min = temp[0]
        if len(temp) > 1 and temp[1].isdecimal():
            deathday_max = temp[1]
        result_list = result_list.filter(Author.death_date != 'N/A')
        result_list = result_list.filter(
            Author.death_date >= deathday_min, Author.death_date <= deathday_max)
    # Expected format: '...&has_website=True'
    has_website = args.get('has_website')
    if has_website != None and has_website != '':
        has_website = has_website.lower().strip()
        if has_website == 'true':
            result_list = result_list.filter(Author.website_link != 'N/A')
        else:
            result_list = result_list.filter(Author.website_link == 'N/A')
    # Expected format: '...&works_range=2-10, assumes '-' is included
    num_works_min = 0
    num_works_max = sys.maxsize
    num_works_str = args.get('works_range')
    if num_works_str != None and num_works_str != '':
        temp = num_works_str.split('-')
        if temp[0].isdecimal():
            num_works_min = temp[0]
        if len(temp) > 1 and temp[1].isdecimal():
            num_works_max = temp[1]
        result_list = result_list.filter(
            Author.num_works <= num_works_max, Author.num_works >= num_works_min)
    search = args.get('q')
    if search is not None:
        search.strip()
    if search != None and search != '':
        result_list = search_author(result_list, search)
    if ordering == None:
        ordering = 'asc'
    if orderBy != None and orderBy != '':
        if orderBy == 'OLID':
            result_list = result_list.filter(Author.OLID != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Author.OLID)
            else:
                result_list = result_list.order_by(Author.OLID.desc())
        elif orderBy == 'name':
            result_list = result_list.filter(Author.name != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Author.name)
            else:
                result_list = result_list.order_by(Author.name.desc())
        elif orderBy == 'top_work':
            if ordering == 'asc':
                result_list = result_list.order_by(Author.top_work)
            else:
                result_list = result_list.order_by(Author.top_work.desc())
        elif orderBy == 'bio':
            result_list = result_list.filter(Author.bio != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Author.bio)
            else:
                result_list = result_list.order_by(Author.bio.desc())
        elif orderBy == 'birth_date':
            result_list = result_list.filter(Author.birth_date != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Author.birth_date)
            else:
                result_list = result_list.order_by(Author.birth_date.desc())
        elif orderBy == 'death_date':
            result_list = result_list.filter(Author.death_date != 'N/A')
            if ordering == 'asc':
                result_list = result_list.order_by(Author.death_date)
            else:
                result_list = result_list.order_by(Author.death_date.desc())
        elif orderBy == 'num_works':
            if ordering == 'asc':
                result_list = result_list.order_by(Author.num_works)
            else:
                result_list = result_list.order_by(Author.num_works.desc())
        elif orderBy == 'website_link':
            if ordering == 'asc':
                result_list = result_list.order_by(Author.website_link)
    else:
        if ordering == 'desc':
            result_list = result_list.order_by(Author.id.desc())
    # Pagination
    per_page = args.get('per_page')
    page = args.get('page')
    if per_page is None:
        per_page = 20
    if page is None:
        page = 1
    count = result_list.count()
    result_page = result_list.paginate(page=int(page), per_page=int(per_page))
    result = author_schema.dump(result_page.items, many=True)
    return jsonify({'count': count, 'results': result})

# GET author given id


@ app.route('/api/author/<int:id>')
def getAuthor(id):
    result = db.session.query(Author).filter_by(id=id).first()
    return jsonify(author_schema.dump(result))

# GET all works


@ app.route('/api/work/all')
def getWorks():
    result_list = db.session.query(Book).all()
    result = []
    for elem in result_list:
        result.append(book_schema.dump(elem))
    # result_json = author_schema.dump(result, many=True)
    return jsonify(result)


# GET work given id
@ app.route('/api/work/<int:id>')
def getWork(id):
    result = db.session.query(Book).filter_by(id=id).first()
    return jsonify(book_schema.dump(result))

# RUN unit tests and give result


@ app.route('/test/unittest')
def getUnitTest():
    # try to load all testcases from given module, hope your testcases are extending from unittest.TestCase
    suite = unittest.TestLoader().loadTestsFromModule(tests)
    # run all tests with verbosity
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    return jsonify({'errors': len(result.errors), 'failures': len(result.failures)})


@ app.route('/api/works', methods=["GET"])
def get_works():
    ranges = {"page_count", "publish_date"}


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
