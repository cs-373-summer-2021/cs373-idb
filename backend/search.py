import sys
from flask import Flask, render_template, url_for, jsonify, request
from sqlalchemy import or_, nullslast
from flask_cors import CORS
from models import  app, db, Author, Book, Publisher, Subjects, AuthorSchema, BookSchema, PublisherSchema
import json
import unittest
import tests


def wildcardify(string):
    return '%' + string + '%'

def search_publisher(query, q):
    if q is None or query is None or q == '':
        return query
    
    search_str = q.lower().strip()
    search_wilded = wildcardify(search_str)
    #Search all attributes for a match return when done
    return query.filter(
        Publisher.name.ilike(search_wilded) 
        | Publisher.status.ilike(search_wilded) 
        | Publisher.website.ilike(search_wilded)
        | Publisher.country.ilike(search_wilded)
        | Publisher.headquarters.ilike(search_wilded)
        | Publisher.founded.ilike(search_wilded))

def search_author(query, q):
    if q is None or query is None or q == '':
        return query
    
    search_str = q.lower().strip()
    search_wilded = wildcardify(search_str)
    #Search all attributes for a match return when done
    #NOTE: doesn't search num_works attribute because its an int
    return query.filter(
        Author.name.ilike(search_wilded)
        | Author.bio.ilike(search_wilded)
        | Author.top_work.ilike(search_wilded)
        | Author.birth_date.ilike(search_wilded)
        | Author.death_date.ilike(search_wilded)
        | Author.website_link.ilike(search_wilded))

def search_work(query, q):
    if q is None or query is None or q == '':
        return query
    
    search_str = q.lower().strip()
    search_wilded = wildcardify(search_str)
    #Search all attributes for a match return when done
    return query.filter(
        Book.title.ilike(search_wilded)
        | Book.subtitle.ilike(search_wilded)
        | Book.description.ilike(search_wilded)
        | Book.isbn.ilike(search_wilded)
        | Book.publish_date.ilike(search_wilded))