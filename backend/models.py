from flask import Flask
import requests
import json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
from marshmallow import fields
import os
from sqlalchemy.orm import column_property
from sqlalchemy.sql.expression import and_, select, true
from sqlalchemy.util.langhelpers import hybridproperty
from sqlalchemy import func 

app = Flask(__name__)
CORS(app)

# - POSTGRES_USER=postgres
# - POSTGRES_PASSWORD=postgres101
# - POSTGRES_DB=postgres
# - PGDATA=/var/lib/postgresql/data/pgdata
# Change this accordingly 
USER ="postgres"
PASSWORD ="postgres101"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="postgres"

# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine

# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # To suppress a warning message
db = SQLAlchemy(app)
ma = Marshmallow(app)

#join tables

BookAuthor = db.Table('BookAuthor', 
                      db.Column('author_id', db.Integer, db.ForeignKey('authors.id')), 
                      db.Column('book_id', db.Integer, db.ForeignKey('books.id')))

PublisherAuthor = db.Table('PublisherAuthor', 
                           db.Column('author_id', db.Integer, db.ForeignKey('authors.id')), 
                           db.Column('publisher_id', db.Integer, db.ForeignKey('publishers.id')))

SubjectBook = db.Table('SubjectBook',
                     db.Column('subject_id', db.Integer, db.ForeignKey('subjects.id')),
                     db.Column('book_id', db.Integer, db.ForeignKey('books.id')))

# Models


class Publisher(db.Model):
    __tablename__ = "publishers" 
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable=False, unique = True)
    status = db.Column(db.String, nullable = False)
    website = db.Column(db.String, nullable = False)
    country = db.Column(db.String, nullable = False)
    headquarters = db.Column(db.String, nullable = False)
    founded =    db.Column(db.String, nullable = False)
    image_link = db.Column(db.String, nullable = False)
    books = db.relationship('Book', backref = 'bookID')
    authors = db.relationship('Author', secondary = 'PublisherAuthor', backref = 'publisherID')


# OLID can be used to source images for books and authors
#See: https://openlibrary.org/dev/docs/api/covers
class Book(db.Model):
    __tablename__ = "books"
    id = db.Column(db.Integer, primary_key = True)
    OLID = db.Column(db.String, nullable = False)
    work_id = db.Column(db.String, nullable = False) 
    title = db.Column(db.String, nullable = False)
    subtitle = db.Column(db.String, nullable = True)
    page_count = db.Column(db.Integer, nullable = False)
    description = db.Column(db.String, nullable = False)
    #default ISBN to 13 if not found use 10 if none input N/A
    isbn = db.Column(db.String, nullable = False)
    publish_date = db.Column(db.String, nullable = False)
    subjects = db.relationship('Subjects', secondary = 'SubjectBook', backref = 'about')
    authors = db.relationship('Author', secondary = 'BookAuthor', back_populates = 'books_list')
    publisher_id = db.Column(db.Integer, db.ForeignKey('publishers.id'))

class Author(db.Model):
    __tablename__ = 'authors'
    id = db.Column(db.Integer, primary_key = True)
    OLID = db.Column(db.String, nullable = False)
    name = db.Column(db.String, nullable = False)
    # Might not have a bio 
    bio = db.Column(db.String, nullable = False)
    # Many authors dont have a top work
    top_work = db.Column(db.String, nullable = False)
    birth_date = db.Column(db.String, nullable = False)
    death_date = db.Column(db.String, nullable = False)
    website_link = db.Column(db.String, nullable = False)
    books_list = db.relationship('Book', secondary = 'BookAuthor', back_populates='authors')
    num_works = column_property(
        select(func.count(Book.id)
        ).where(
            and_(
                BookAuthor.c.author_id==id,
                BookAuthor.c.book_id==Book.id
            )
        ).scalar_subquery()
    , deferred = True)
    # books = db.relationship('Book', secondary = 'BookAuthor', backref = 'writer')
    # publishers = db.relationship('Author', secondary = 'PublisherAuthor', backref = 'publishes')

    
class Subjects(db.Model):
    __tablename__ = 'subjects'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False )
    # books = db.relationship('Book', secondary = 'SubjectBook', backref = 'books')

class PublisherSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Publisher
        include_relationships = True
        load_instance = True
        include_fk = True

class AuthorSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Author
        include_relationships = True
        load_instance = True
        include_fk = True
    num_works = fields.Integer(dump_only=true)


class BookSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Book
        include_relationships = True
        load_instance = True
        include_fk = True

class SubjectsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Subjects
        include_relationships = True
        load_instance = True
        include_fk = True




if __name__ == "__main__":
    db.drop_all()
    db.create_all()
