from unittest import main, TestCase
import requests
import json
from models import app, db, Author, Book, Publisher, Subjects 


# baseURL = "https://cs373-idb-317819.uc.r.appspot.com/api"
baseURL = "http://localhost:5000/api"

class tests(TestCase):

    @classmethod
    def setUpClass(self):
        self.author_response_all = requests.get("{}/author/all".format(baseURL))
        self.author_all_json = self.author_response_all.json()
        self.author_response_id = requests.get("{}/author/1".format(baseURL))
        self.author_id_json = self.author_response_id.json()
        self.publisher_response_all = requests.get("{}/publisher/all".format(baseURL))
        self.publisher_all_json = self.publisher_response_all.json()
        self.publisher_response_id = requests.get("{}/publisher/1".format(baseURL))
        self.publisher_id_json = self.publisher_response_id.json()
        self.work_response_all = requests.get("{}/work/all".format(baseURL))
        self.work_all_json = self.work_response_all.json()
        self.work_response_id = requests.get("{}/work/1".format(baseURL))
        self.work_id_json = self.work_response_id.json()
        my_params = {'founded' : '1945-2011', 'orderBy' : 'founded'}
        self.publisher_filter_one = requests.get(baseURL + '/publisher/all/sorted', params=my_params)
        self.publisher_filter_one_json = self.publisher_filter_one.json()
        my_params = {'has_website': 'True'}
        self.author_filter_one = requests.get(baseURL + '/author/all/sorted', params=my_params)
        self.author_filter_one_json = self.publisher_filter_one.json()
        searcher = {'q' : 'kodansha'}
        searcher2 = {'q' : 'paddington'}
        self.publisher_searcher_one = requests.get(baseURL + '/publisher/all/sorted', params = searcher)
        self.publisher_searcher_one_json = self.publisher_searcher_one.json()
        self.author_searcher_one = requests.get(baseURL + '/author/all/sorted', params = searcher2)


    # --- Database Tests ---

    # Test: - Author - Insert and Remove
    def test_author_db(self):
        test = Author(OLID='example', name='example', bio='example', top_work='example', birth_date="example", death_date='example', num_works=0, website_link='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Author).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Author).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Publisher - Insert and Remove
    def test_publisher_db(self):
        test = Publisher(name='example', status='example', website='example', country='example', headquarters='example', founded='example', image_link='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Publisher).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Publisher).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Book - Insert and Remove
    def test_book_db(self):
        test = Book(OLID='example', work_id='example', title='example', subtitle='example', page_count=0, description='example', isbn='example', publish_date='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Book).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Book).filter_by(id = test.id).delete()
        db.session.commit()

    # Test: - Subjects - Insert and Remove
    def test_subject_db(self):
        test = Subjects(name='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Subjects).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Subjects).filter_by(id = test.id).delete()
        db.session.commit()

    # --- API Tests ---

    # Multiple Tests


    # Test: - Attributes Exist Test

    def test_author_all_response_200(self):
        self.assertEqual(self.author_response_all.status_code, 200)

    def test_author_all_result_1(self):
        sample = self.author_all_json[0]
        self.assertTrue("id" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("bio" in sample.keys())
        self.assertTrue("top_work" in sample.keys())
        self.assertTrue("birth_date" in sample.keys())
        self.assertTrue("death_date" in sample.keys())
        self.assertTrue("num_works" in sample.keys())
        self.assertTrue("website_link" in sample.keys())
        self.assertTrue("OLID" in sample.keys())

        

    # Authors API Test
        

    # Test: - Attributes Exist Test

    def test_author_id_response_200(self):
        self.assertEqual(self.author_response_id.status_code, 200)

    def test_author_id_id(self):
        self.assertTrue("id" in self.author_id_json.keys())

    def test_author_id_name(self):
        self.assertTrue("name" in self.author_id_json.keys())
    
    def test_author_id_shortDescription(self):
        self.assertTrue("bio" in self.author_id_json.keys())
    
    def test_author_id_top_work(self):
        self.assertTrue("top_work" in self.author_id_json.keys())

    def test_author_id_birthday(self):
        self.assertTrue("birth_date" in self.author_id_json.keys())

    def test_author_id_deathday(self):
        self.assertTrue("death_date" in self.author_id_json.keys())
    
    def test_author_id_num_works(self):
        self.assertTrue("num_works" in self.author_id_json.keys())
    
    def test_author_id_website_link(self):
        self.assertTrue("website_link" in self.author_id_json.keys())

    def test_author_id_imageLink(self):
        self.assertTrue("OLID" in self.author_id_json.keys())
        
    # Publishers API Test

    # Multiple Tests


    # Test: - Attributes Exist Test

    def test_publisher_all_response_200(self):
        self.assertEqual(self.publisher_response_all.status_code, 200)

    def test_publisher_all_result_1(self):
        sample = self.publisher_all_json[0]
        self.assertTrue("id" in sample.keys())
        self.assertTrue("name" in sample.keys())
        self.assertTrue("status" in sample.keys())
        self.assertTrue("website" in sample.keys())
        self.assertTrue("country" in sample.keys())
        self.assertTrue("headquarters" in sample.keys())
        self.assertTrue("founded" in sample.keys())
        self.assertTrue("books" in sample.keys())
        self.assertTrue("authors" in sample.keys())

    # ID Tests


    # Test: - Attributes Exist Test

    def test_publisher_id_response_200(self):
        self.assertEqual(self.publisher_response_id.status_code, 200)

    def test_publisher_id_id(self):
        self.assertTrue("id" in self.publisher_id_json.keys())

    def test_publisher_id_name(self):
        self.assertTrue("name" in self.publisher_id_json.keys())
    
    def test_publisher_id_websiteLink(self):
        self.assertTrue("status" in self.publisher_id_json.keys())

    def test_publisher_id_websiteLink(self):
        self.assertTrue("website" in self.publisher_id_json.keys())

    def test_publisher_id_country(self):
        self.assertTrue("country" in self.publisher_id_json.keys())
    
    def test_publisher_id_websiteLink(self):
        self.assertTrue("headquarters" in self.publisher_id_json.keys())

    def test_publisher_id_founded(self):
        self.assertTrue("founded" in self.publisher_id_json.keys())

    def test_publisher_id_notableBooks(self):
        self.assertTrue("books" in self.publisher_id_json.keys())
    
    def test_publisher_id_notableAuthors(self):
        self.assertTrue("authors" in self.publisher_id_json.keys())

    # Works API Test

    # Multiple Tests


    # Test: - Attributes Exist Test

    def test_work_all_response_200(self):
        self.assertEqual(self.work_response_all.status_code, 200)

    def test_work_all_result(self):
        sample = self.work_all_json[0]
        self.assertTrue("id" in sample.keys())
        self.assertTrue("work_id" in sample.keys())
        self.assertTrue("title" in sample.keys())
        self.assertTrue("subtitle" in sample.keys())
        self.assertTrue("page_count" in sample.keys())
        self.assertTrue("description" in sample.keys())
        self.assertTrue("isbn" in sample.keys())
        self.assertTrue("publish_date" in sample.keys())
        self.assertTrue("OLID" in sample.keys())
        self.assertTrue("subjects" in sample.keys())
        self.assertTrue("authors" in sample.keys())
        self.assertTrue("publisher_id" in sample.keys())

    # ID Tests


    # Test: - Attributes Exist Test

    def test_work_id_response_200(self):
        self.assertEqual(self.work_response_id.status_code, 200)

    def test_work_id_id(self):
        self.assertTrue("id" in self.work_id_json.keys())
    
    def test_work_id_id(self):
        self.assertTrue("work_id" in self.work_id_json.keys())

    def test_work_id_title(self):
        self.assertTrue("title" in self.work_id_json.keys())

    def test_work_id_subtitle(self):
        self.assertTrue("subtitle" in self.work_id_json.keys())

    def test_work_id_pageCount(self):
        self.assertTrue("page_count" in self.work_id_json.keys())

    def test_work_id_author(self):
        self.assertTrue("description" in self.work_id_json.keys())

    def test_work_id_publisher(self):
        self.assertTrue("isbn" in self.work_id_json.keys())

    def test_work_id_publishDate(self):
        self.assertTrue("publish_date" in self.work_id_json.keys())
    
    def test_work_id_imageLink(self):
        self.assertTrue("OLID" in self.work_id_json.keys())
    
    def test_work_id_imageLink(self):
        self.assertTrue("subjects" in self.work_id_json.keys())
    
    def test_work_id_imageLink(self):
        self.assertTrue("authors" in self.work_id_json.keys())
    
    def test_work_id_imageLink(self):
        self.assertTrue("publisher_id" in self.work_id_json.keys())
    
    #Test: publisher filtering
    def test_publisher_filter_1(self):
        self.assertTrue(self.publisher_filter_one.status_code == 200)

    def test_publisher_filter_2(self):
        self.assertTrue(len(self.publisher_filter_one.text) > 3)

    def test_publisher_filter_3(self):
        # Founded 1945-2011
        test = True
        results = self.publisher_filter_one_json['results']
        for res in results:
            if res.get('founded') is None:
                test = False
        self.assertTrue(test)
    
    def test_author_filter_1(self):
        self.assertTrue(self.author_filter_one.status_code == 200)

    def test_author_filter_2(self):
        self.assertTrue(len(self.author_filter_one.text) > 3)
    # def test_author_filter_2(self):
    #     test = True
    #     results = self.publisher_filter_one_json['results']
    #     print(results.keys())
    #     self.assert(test)    



    # Test: - Search tests
    def test_publisher_searcher_1(self):
        self.assertTrue(self.publisher_searcher_one.status_code == 200)
    
    def test_publisher_searcher_2(self):
        self.assertTrue(len(self.publisher_searcher_one.text) != 0)
    
    def test_publisher_searcher_3(self):
        self.assertTrue('kodansha' in self.publisher_searcher_one.text)
     

    def test_author_searcher_1(self):
        self.assertTrue(self.author_searcher_one.status_code == 200)
    
    def test_author_searcher_2(self):
        self.assertTrue(len(self.author_searcher_one.text) != 0)
    
    def test_author_searcher_3(self):
        self.assertTrue('Paddington' in self.author_searcher_one.text)
    




if __name__ == "__main__":
    main()
