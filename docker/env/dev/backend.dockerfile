FROM ubuntu:20.04
LABEL "MAINTAINER"="Erik Bari Dos Reis"

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

RUN DEBIAN_FRONTEND=noninteractive apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y git && \
    apt install -y build-essential && \
    apt install -y python3.8 && \
    apt install -y python3-pip && \
    apt install -y python3-venv && \
    apt install -y python3-dev && \
    apt install -y libpq-dev && \
    apt clean
RUN git config --global user.email "erik.bari@mac.com" && \
    git config --global user.name "Erik Bari Dos Reis"

RUN mkdir -p /opt/cs373/website/backend
# Copy backend folder ("./backend") to image
# folder ("/opt/cs373/website/")
COPY ./backend/requirements.txt /opt/cs373/website/backend/requirements.txt
COPY ./backend/dev-requirements.txt /opt/cs373/website/backend/dev-requirements.txt

# Choose working directory
WORKDIR /opt/cs373/website/backend

# Install dependencies of flask/uvicorn
RUN python3 -m pip install --upgrade pip && \
    pip3 install --upgrade cython && \
    pip3 install wheel && \
    pip3 install -r dev-requirements.txt

# Export port 5000 for flask/uvicorn
EXPOSE 5000 1234

# ENTRYPOINT ["/bin/sh", "-c"]
# CMD ["bash"]

ENTRYPOINT ["python3"]
CMD ["main.py"]