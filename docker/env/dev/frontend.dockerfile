FROM node:14
LABEL "MAINTAINER"="Erik Bari Dos Reis"


# RUN apt-get install --yes git &&
# RUN git config --global user.email "erik.bari@mac.com" && \
#     git config --global user.name "Erik Bari Dos Reis"
RUN mkdir -p /opt/cs373/websitefrontend
# Copy frontend folder ("../../../frontend") to image
# folder ("/opt/cs373/website/")
COPY ./frontend/package.json /opt/cs373/website/frontend/package.json
COPY ./frontend/yarn.lock /opt/cs373/website/frontend/yarn.lock
# Choose working directory
WORKDIR /opt/cs373/website/frontend
# Install yarn dependencies
RUN yarn install
RUN yarn
# Expose port for yarn
EXPOSE 3000

ENTRYPOINT ["yarn", "start"]