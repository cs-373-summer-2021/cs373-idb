import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { CssBaseline } from '@material-ui/core';
import GlobalStyleProvider from './styles/global';
import Header from './components/Header';
import Footer from './components/Footer';
import Routes from './routes/routes';
import BaseThemeProvider from './themes';

const App: React.FC = () => {
  return (
    <GlobalStyleProvider>
      <CssBaseline />
      <BaseThemeProvider>
        <BrowserRouter>
          <Header />
          <Routes />
          <Footer />
        </BrowserRouter>
      </BaseThemeProvider>
    </GlobalStyleProvider>
  );
};

export default App;
