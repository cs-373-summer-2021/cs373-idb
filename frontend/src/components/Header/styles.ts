import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // display: 'flex',
      maxHeight: '32px',
    },
    toolBar: {
      minHeight: '32px',
      backgroundColor: theme.customPalette.common.green,
    },
    appBar: {},
    offset: theme.mixins.toolbar,
    img: {
      width: 'auto',
      height: 32,

      // marginTop: theme.spacing(0.5),
      paddingRight: theme.spacing(4),
    },
    navitem: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
      textDecoration: 'none',
      color: 'white',
    },
    title: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',

      textDecoration: 'none',
      color: 'white',

      paddingTop: theme.spacing(0.5),
      paddingBottom: theme.spacing(0.5),
    },
    link: {
      textDecoration: 'none',
    },
  });
});

export default useStyles;
