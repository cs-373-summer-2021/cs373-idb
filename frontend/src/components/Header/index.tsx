import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { AppBar, Toolbar, Grid, Typography, Input } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import bookIcon from '../../assets/book.svg';
import useStyles from './styles';

const Header: React.FC = () => {
  const classes = useStyles();
  const [searchQuery, setSearchQuery] = useState('');
  const handleChange = e => {
    // console.log(`Typed => ${e.target.value}`);
    setSearchQuery(e.target.value);
  };
  function setURL() {
    window.location.href = `/search/${searchQuery}`;
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          <Link to="/" className={classes.title}>
            <img src={bookIcon} alt="logo" className={classes.img} />
            <Typography variant="h5">BookSleuth</Typography>
          </Link>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
          >
            <Link to="/publishers" className={classes.navitem}>
              Publishers
            </Link>
            <Link to="/authors" className={classes.navitem}>
              Authors
            </Link>
            <Link to="/works" className={classes.navitem}>
              Works
            </Link>
            <Link to="/about" className={classes.navitem}>
              About
            </Link>
            <Link to="/politics" className={classes.navitem}>
              Politics In Review
            </Link>
          </Grid>
          <Link to={`/search/${searchQuery}`} className={classes.link}>
            <Button
              color="inherit"
              aria-label="search"
              component="span"
              // href={`/search/${searchQuery}`}
              onClick={() => {
                setURL();
              }}
            >
              <SearchIcon />
            </Button>
          </Link>
          <Input
            type="search"
            placeholder="Search"
            value={searchQuery}
            onChange={handleChange}
          />
        </Toolbar>
      </AppBar>
      <div className={classes.offset} />
    </div>
  );
};

export default Header;
