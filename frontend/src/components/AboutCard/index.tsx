import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles';
import { AboutCardProps } from './types';

const AboutCard: React.FC<AboutCardProps> = props => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  // -- card information --
  const {
    name,
    job,
    bio,
    responsibilities,
    commits,
    issues,
    unitTests,
    photo,
  } = props;

  return (
    <Card className={classes.root}>
      <div className={classes.main}>
        <CardMedia className={classes.media} image={photo} title="Image" />
      </div>
      <CardHeader title={name} subheader={job} />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {bio}
        </Typography>
      </CardContent>
      <CardContent>
        <Typography paragraph variant="body1" color="textPrimary" component="p">
          {responsibilities}
        </Typography>
        <Typography
          paragraph
          variant="body2"
          color="textSecondary"
          component="p"
          align="left"
        >
          {`Commits: ${commits} Issues: ${issues} Unit Tests: ${unitTests}`}
        </Typography>
      </CardContent>
    </Card>
  );
};

AboutCard.propTypes = {
  name: PropTypes.string.isRequired,
  job: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  responsibilities: PropTypes.string.isRequired,
  commits: PropTypes.number.isRequired,
  issues: PropTypes.number.isRequired,
  unitTests: PropTypes.number.isRequired,
  photo: PropTypes.string.isRequired,
};

export default AboutCard;
