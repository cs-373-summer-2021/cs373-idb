export interface AboutCardProps {
  name: string;
  job: string;
  bio: string;
  responsibilities: string;
  commits: number;
  issues: number;
  unitTests: number;
  photo: string;
}
