import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 200,
      // maxWidth: 345,
      // minWidth: 200,
      width: 200,
      minHeight: 1045,
    },
    media: {
      width: 200,
      height: 300,
      objectFit: 'contain',
    },
    main: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  });
});

export default useStyles;
