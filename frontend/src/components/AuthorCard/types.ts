/* eslint-disable camelcase */
export interface AuthorCardProps {
  id: number;
  name: string;
  bio: string;
  top_work: string;
  birth_date: string;
  death_date: string;
  num_works: number;
  website_link: string;
  OLID: string;
  books_list: number[];
  publisherID: number[];
  searchQuery: string;
}
