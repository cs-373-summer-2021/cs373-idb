import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 200,
      // maxWidth: 345,
      height: 600,
      width: 300,
    },
    media: {
      width: 300,
      height: 300,
      objectFit: 'contain',
    },
    verticalOffset: {
      marginTop: theme.spacing(1),
      // marginBottom: theme.spacing(1),
    },
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      textDecoration: 'none',
    },
    biography: {
      display: '-webkit-box',
      boxOrient: 'vertical',
      lineClamp: 5,
      wordBreak: 'break-all',
      overflow: 'hidden',
    },
  });
});

export default useStyles;
