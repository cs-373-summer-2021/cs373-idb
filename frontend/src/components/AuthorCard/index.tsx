/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import useStyles from './styles';
import { AuthorCardProps } from './types';
import getFirstThree from '../../utils';
import BookTitle from '../BookTitle';
import PublisherName from '../PublisherName';
import ImageOLID from '../ImageOLID';
import TextHighlight from '../TextHighlight';

const AuthorCard: React.FC<AuthorCardProps> = props => {
  const classes = useStyles();

  // -- card information --
  const {
    id,
    name,
    bio,
    top_work,
    birth_date,
    death_date,
    num_works,
    website_link,
    OLID,
    books_list,
    publisherID,
    searchQuery,
  } = props;

  const searchWords = searchQuery ? [searchQuery] : [];

  return (
    <Link to={`authors/${id}`} className={classes.infoLink}>
      <Card className={classes.root}>
        <ImageOLID OLID={OLID} model="a" />
        <CardHeader title={name} />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`${birth_date} -${death_date}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Number of works: ${num_works}`}
            />
          </Typography>
          <a href={website_link} className={classes.link}>
            <Typography variant="body2" color="textSecondary" component="p">
              <TextHighlight
                searchWords={searchWords}
                textToHighlight={`Website: ${website_link}`}
              />
            </Typography>
          </a>
          <br />
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.biography}
          >
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Bio: ${bio}`}
            />
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Works:
            {getFirstThree(books_list).map((givenID: number) => {
              return <BookTitle bookID={givenID} searchWords={searchWords} />;
            })}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Notable Work: ${top_work}`}
            />
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Publishers:
            {getFirstThree(publisherID).map((givenID: number) => {
              return (
                <PublisherName
                  publisherID={givenID}
                  searchWords={searchWords}
                />
              );
            })}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

AuthorCard.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  top_work: PropTypes.string.isRequired,
  birth_date: PropTypes.string.isRequired,
  death_date: PropTypes.string.isRequired,
  num_works: PropTypes.number.isRequired,
  website_link: PropTypes.string.isRequired,
  OLID: PropTypes.string.isRequired,
  books_list: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  publisherID: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default AuthorCard;
