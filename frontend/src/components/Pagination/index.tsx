import React from 'react';
import PropTypes from 'prop-types';
import TablePagination from '@material-ui/core/TablePagination';
import useStyles from './styles';

interface PaginationProps {
  count: number;
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  rowsPerPage: number;
  setRowsPerPage: React.Dispatch<React.SetStateAction<number>>;
  labelRowsPerPage: string;
}

const Pagination: React.FC<PaginationProps> = props => {
  const classes = useStyles();
  const {
    count,
    page,
    setPage,
    rowsPerPage,
    setRowsPerPage,
    labelRowsPerPage,
  } = props;

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number,
  ) => {
    setPage(newPage + 1);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
  };

  return (
    <TablePagination
      rowsPerPageOptions={[6, 15, 45]}
      component="div"
      count={count}
      rowsPerPage={rowsPerPage}
      page={page - 1}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
      labelRowsPerPage={labelRowsPerPage}
    />
  );
};

Pagination.propTypes = {
  count: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  setRowsPerPage: PropTypes.func.isRequired,
  labelRowsPerPage: PropTypes.string.isRequired,
};

export default Pagination;
