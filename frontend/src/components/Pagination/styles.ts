import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {},
    table: {
      minWidth: 650,
    },
  });
});

export default useStyles;
