import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {},
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      // color: 'white',
      // marginTop: theme.spacing(0.2),
      textDecoration: 'none',
    },
  });
});

export default useStyles;
