/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { PublisherCardProps } from './types';
import bookIcon from '../../../assets/book.svg';
import getFirstThree from '../../../utils';
import { AuthorCardAllProps, WorkCardAllProps } from '../../Info/types';
import { urls } from '../../../pages/InfoPages/constants';
import { api } from '../../../services';
import BookTitle from '../../BookTitle';
import AuthorName from '../../AuthorName';
import TextHighlight from '../../TextHighlight';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 200,
      // maxWidth: 345,
      minHeight: 500,
      width: 300,
    },
    media: {
      // height: 0,
      // width: '100%',
      // paddingTop: '100%',
      width: 300,
      height: 300,
      objectFit: 'contain',
    },
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      textDecoration: 'none',
    },
  });
});

const PublisherPreview: React.FC<PublisherCardProps> = props => {
  const classes = useStyles();

  // -- card information --
  const {
    id,
    name,
    status,
    founded,
    country,
    headquarters,
    website,
    books,
    authors,
    image_link,
    searchQuery,
  } = props;

  const searchWords = searchQuery ? [searchQuery] : [];

  return (
    <Link to={`/publishers/${id}`} className={classes.infoLink}>
      <Card className={classes.root}>
        <CardMedia
          component="img"
          className={classes.media}
          image={image_link}
          title="Image"
        />
        <CardHeader title={name} />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Founded: ${founded}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Status: ${status}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Country: ${country}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Headquarters: ${headquarters}`}
            />
          </Typography>
          <a href={website} className={classes.link}>
            <Typography variant="body2" color="textSecondary" component="p">
              <TextHighlight
                searchWords={searchWords}
                textToHighlight={`Website: ${website}`}
              />
            </Typography>
          </a>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Books:
            {getFirstThree(books).map((givenID: number) => {
              return <BookTitle bookID={givenID} searchWords={searchWords} />;
            })}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Authors:
            {getFirstThree(authors).map((givenID: number) => {
              return (
                <AuthorName authorID={givenID} searchWords={searchWords} />
              );
            })}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

PublisherPreview.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  founded: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  headquarters: PropTypes.string.isRequired,
  website: PropTypes.string.isRequired,
  books: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  authors: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  image_link: PropTypes.string.isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default PublisherPreview;
