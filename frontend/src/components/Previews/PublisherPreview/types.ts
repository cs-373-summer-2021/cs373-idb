/* eslint-disable camelcase */
export interface PublisherCardProps {
  id: number;
  name: string;
  status: string;
  founded: string;
  country: string;
  headquarters: string;
  website: string;
  books: number[];
  authors: number[];
  image_link: string;
  searchQuery: string;
}
