/* eslint-disable camelcase */
export interface WorkCardProps {
  id: number;
  title: string;
  subtitle: string | null;
  page_count: number;
  description: string;
  isbn: string;
  publish_date: string;
  OLID: string;
  subjects: number[];
  authors: number[];
  publisher_id: number;
  searchQuery: string;
}
