import React from 'react';

import useStyles from './styles';
import bookIcon from '../../assets/book.svg';

const Footer: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <img src={bookIcon} alt="logo" className={classes.media} />
        <div>
          Icons made by
          <a href="https://www.freepik.com" title="Freepik">
            Freepik
          </a>
          from
          <a href="https://www.flaticon.com/" title="Flaticon">
            www.flaticon.com
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
