import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minHeight: '20vh',
      position: 'static',
      bottom: 0,
      left: 0,
      width: '100%',
    },
    header: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      backgroundColor: theme.customPalette.common.green,

      // marginTop: theme.spacing(2),
    },
    media: {
      width: 'auto',
      height: 24,
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  });
});

export default useStyles;
