import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { Button } from '@material-ui/core';
import useStyles from './styles';

interface SearchBarProps {
  setQuery: React.Dispatch<React.SetStateAction<string>>;
}

const SearchBar: React.FC<SearchBarProps> = props => {
  const { setQuery } = props;

  const classes = useStyles();
  const [searchFilter, setSearchFilter] = useState('');

  const handleSubmit = () => {
    setQuery(searchFilter);
  };

  const handleSearchChange = e => {
    setSearchFilter(e.target.value);
  };

  return (
    <div className={classes.root}>
      <Toolbar>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <Button
              aria-label="search"
              component="span"
              variant="outlined"
              color="primary"
              onClick={handleSubmit}
            >
              <SearchIcon />
            </Button>
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ 'aria-label': 'search' }}
            onChange={handleSearchChange}
          />
        </div>
      </Toolbar>
    </div>
  );
};

SearchBar.propTypes = {
  setQuery: PropTypes.func.isRequired,
};

export default SearchBar;
