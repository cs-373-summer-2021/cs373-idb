import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { urls } from '../../pages/InfoPages/constants';
import { api } from '../../services';
import useStyles from './styles';
import TextHighlight from '../TextHighlight';

interface AuthorTitleProps {
  authorID: number;
  searchWords: string[];
}

const AuthorName: React.FC<AuthorTitleProps> = props => {
  const classes = useStyles();
  const { authorID, searchWords } = props;
  const [authorName, setAuthorName] = useState('N/A');

  useEffect(() => {
    const authorPath = `${urls.authorId}${authorID}`;
    api
      .get(authorPath)
      .then(value => {
        // console.log(value);
        setAuthorName(value.data.name);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <Link to={`../authors/${authorID}`} className={classes.link}>
      <TextHighlight
        searchWords={searchWords}
        textToHighlight={` ${authorName}`}
      />
    </Link>
  );
};

AuthorName.propTypes = {
  authorID: PropTypes.number.isRequired,
  searchWords: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default AuthorName;
