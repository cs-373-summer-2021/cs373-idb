import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 200,
      // maxWidth: 345,
      minHeight: 600,
      width: 300,
    },
    media: {
      // height: 0,
      // width: '100%',
      // paddingTop: '100%',
      width: 300,
      height: 300,
      objectFit: 'contain',
    },
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      textDecoration: 'none',
    },
  });
});

export default useStyles;
