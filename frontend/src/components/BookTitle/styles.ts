import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    text: {
      color: 'white',
    },
    link: {
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      color: 'white',
      textDecoration: 'none',
    },
  });
});

export default useStyles;
