import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { urls } from '../../pages/InfoPages/constants';
import { api } from '../../services';
import useStyles from './styles';
import TextHighlight from '../TextHighlight';

interface BookTitleProps {
  bookID: number;
  searchWords: string[];
}

const BookTitle: React.FC<BookTitleProps> = props => {
  const classes = useStyles();
  const { bookID, searchWords } = props;
  const [bookTitle, setBookTitle] = useState('N/A');

  useEffect(() => {
    const workPath = `${urls.workId}${bookID}`;
    api
      .get(workPath)
      .then(value => {
        // console.log(value);
        setBookTitle(value.data.title);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <Link to={`../works/${bookID}`} className={classes.link}>
      <TextHighlight
        searchWords={searchWords}
        textToHighlight={` ${bookTitle}`}
      />
    </Link>
  );
};

BookTitle.propTypes = {
  bookID: PropTypes.number.isRequired,
  searchWords: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default BookTitle;
