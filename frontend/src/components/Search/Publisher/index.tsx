/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import useStyles from '../styles';
import { PreviewProps } from '../types';
import Pagination from '../../Pagination';
import Progress from '../../Progress';
import SortingBar from '../../SortingBar';
import { api } from '../../../services';
import { PublisherCardProps } from '../../Previews/PublisherPreview/types';
import PublisherPreview from '../../Previews/PublisherPreview';

const PublisherSearch: React.FC<PreviewProps> = props => {
  const classes = useStyles();

  const { searchQuery } = props;
  const [page, setPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const [count, setCount] = useState(10);
  const [isLoading, setIsLoading] = useState(true);
  const columns = [
    'Name (A-Z)',
    'Status',
    'Date Founded',
    'Country',
    'Headquarters',
  ];
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'name';
    }
    if (givenOrderBy === columns[1]) {
      return 'status';
    }
    if (givenOrderBy === columns[2]) {
      return 'founded';
    }
    if (givenOrderBy === columns[3]) {
      return 'country';
    }
    return 'headquarters';
  };

  const [data, setData] = useState([]);

  useEffect(() => {
    const path = 'api/publisher/all/sorted';
    api
      .get(path, {
        params: {
          orderBy: getOrderBy(orderBy),
          ordering,
          q: searchQuery,
          page,
          per_page: rowsPerPage,
        },
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })
      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, page, rowsPerPage]);

  return (
    <div className={classes.root}>
      <h1>Publishers</h1>
      <SortingBar
        columns={columns}
        setOrderBy={setOrderBy}
        options={options}
        setOrdering={setOrdering}
      />
      {isLoading ? (
        <Progress />
      ) : (
        <div className={classes.container}>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="center"
            className={classes.gridContainer}
          >
            {data &&
              data.map((publisher: PublisherCardProps) => {
                return (
                  <Grid
                    item
                    xs={4}
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <PublisherPreview
                      id={publisher.id}
                      name={publisher.name}
                      status={publisher.status}
                      founded={publisher.founded}
                      country={publisher.country}
                      headquarters={publisher.headquarters}
                      website={publisher.website}
                      books={publisher.books}
                      authors={publisher.authors}
                      image_link={publisher.image_link}
                      searchQuery={searchQuery}
                    />
                  </Grid>
                );
              })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Publishers per page"
          />
        </div>
      )}
    </div>
  );
};

PublisherSearch.propTypes = {
  searchQuery: PropTypes.string.isRequired,
};

export default PublisherSearch;
