/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import useStyles from '../styles';
import { PreviewProps } from '../types';
import { api } from '../../../services';
import Progress from '../../Progress';
import WorkPreview from '../../Previews/WorkPreview';
import { WorkCardProps } from '../../Previews/WorkPreview/types';
import Pagination from '../../Pagination';
import SortingBar from '../../SortingBar';

const WorkSearch: React.FC<PreviewProps> = props => {
  const { searchQuery } = props;

  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const [count, setCount] = useState(10);
  const [isLoading, setIsLoading] = useState(true);
  const columns = [
    'Title (A-Z)',
    'Subtitle',
    'Page Count',
    'ISBN',
    'Date of Publication',
  ];
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc

  const [data, setData] = useState([]);

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'title';
    }
    if (givenOrderBy === columns[1]) {
      return 'subtitle';
    }
    if (givenOrderBy === columns[2]) {
      return 'page_count';
    }
    if (givenOrderBy === columns[3]) {
      return 'isbn';
    }
    return 'publish_date';
  };

  useEffect(() => {
    const path = 'api/work/all/sorted';
    api
      // .get(path, {params: { orderBy: getOrderBy(orderBy), ordering, q },
      .get(path, {
        params: {
          orderBy: getOrderBy(orderBy),
          ordering,
          q: searchQuery,
          page,
          per_page: rowsPerPage,
        },
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })
      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, page, rowsPerPage]);

  return (
    <div className={classes.root}>
      <h1>Works</h1>
      <SortingBar
        columns={columns}
        setOrderBy={setOrderBy}
        options={options}
        setOrdering={setOrdering}
      />
      {isLoading ? (
        <Progress />
      ) : (
        <div className={classes.container}>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="flex-start"
            className={classes.gridContainer}
          >
            {data &&
              data.map((work: WorkCardProps) => {
                return (
                  <Grid
                    item
                    xs={4}
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                    zeroMinWidth
                  >
                    <WorkPreview
                      id={work.id}
                      title={work.title}
                      subtitle={work.subtitle}
                      page_count={work.page_count}
                      description={work.description}
                      isbn={work.isbn}
                      publish_date={work.publish_date}
                      OLID={work.OLID}
                      subjects={work.subjects}
                      authors={work.authors}
                      publisher_id={work.publisher_id}
                      searchQuery={searchQuery}
                    />
                  </Grid>
                );
              })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Books per page"
          />
        </div>
      )}
    </div>
  );
};

WorkSearch.propTypes = {
  searchQuery: PropTypes.string.isRequired,
};

export default WorkSearch;
