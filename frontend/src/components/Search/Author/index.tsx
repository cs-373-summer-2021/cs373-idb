/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import useStyles from '../styles';
import { PreviewProps } from '../types';
import { api } from '../../../services';
import SortingBar from '../../SortingBar';
import Progress from '../../Progress';
import AuthorCard from '../../AuthorCard';
import { AuthorCardProps } from '../../AuthorCard/types';
import Pagination from '../../Pagination';
import AuthorPreview from '../../Previews/AuthorPreview';

const AuthorSearch: React.FC<PreviewProps> = props => {
  const { searchQuery } = props;
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [count, setCount] = useState(10);
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const columns = ['Name (A-Z)', 'Birthday', 'Deathday', 'Number of Works'];
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'name';
    }
    if (givenOrderBy === columns[1]) {
      return 'birth_date';
    }
    if (givenOrderBy === columns[2]) {
      return 'death_date';
    }
    return 'num_works';
  };

  useEffect(() => {
    const path = 'api/author/all/sorted';
    api
      // .get(path, {params: { orderBy: getOrderBy(orderBy), ordering, q },
      .get(path, {
        params: {
          orderBy: getOrderBy(orderBy),
          ordering,
          q: searchQuery,
          page,
          per_page: rowsPerPage,
        },
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })
      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, page, rowsPerPage]);

  return (
    <div className={classes.root}>
      <h1>Authors</h1>
      <SortingBar
        columns={columns}
        setOrderBy={setOrderBy}
        options={options}
        setOrdering={setOrdering}
      />
      {isLoading ? (
        <Progress />
      ) : (
        <div className={classes.container}>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="center"
            className={classes.gridContainer}
          >
            {data &&
              data.map((author: AuthorCardProps) => {
                return (
                  <Grid
                    item
                    xs={4}
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <AuthorPreview
                      id={author.id}
                      name={author.name}
                      bio={author.bio}
                      top_work={author.top_work}
                      birth_date={author.birth_date}
                      death_date={author.death_date}
                      num_works={author.num_works}
                      website_link={author.website_link}
                      OLID={author.OLID}
                      books_list={author.books_list}
                      publisherID={author.publisherID}
                      searchQuery={searchQuery}
                    />
                  </Grid>
                );
              })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Authors per page"
          />
        </div>
      )}
    </div>
  );
};

AuthorSearch.propTypes = {
  searchQuery: PropTypes.string.isRequired,
};

export default AuthorSearch;
