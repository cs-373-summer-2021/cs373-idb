import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { urls } from '../../pages/InfoPages/constants';
import { api } from '../../services';
import useStyles from './styles';
import TextHighlight from '../TextHighlight';

interface BookTitleProps {
  publisherID: number;
  searchWords: string[];
}

const PublisherName: React.FC<BookTitleProps> = props => {
  const classes = useStyles();
  const { publisherID, searchWords } = props;
  const [publisherName, setPublisherName] = useState('N/A');

  useEffect(() => {
    const publisherPath = `${urls.publisherId}${publisherID}`;
    api
      .get(publisherPath)
      .then(value => {
        // console.log(value);
        setPublisherName(value.data.name);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <Link to={`../publishers/${publisherID}`} className={classes.link}>
      <TextHighlight
        searchWords={searchWords}
        textToHighlight={` ${publisherName}`}
      />
    </Link>
  );
};

PublisherName.propTypes = {
  publisherID: PropTypes.number.isRequired,
  searchWords: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default PublisherName;
