import React from 'react';
import PropTypes from 'prop-types';
import useStyles from './styles';

interface ImageOLIDProps {
  OLID: string;
  model: string;
}

const ImageOLID: React.FC<ImageOLIDProps> = props => {
  const classes = useStyles();
  const { OLID, model } = props; // model should be either 'a' or 'b'
  const olid =
    model === 'b' ? OLID.replace('/books/', '') : OLID.replace('/authors/', '');
  const c = '-L.jpg';
  const imgLink = `http://covers.openlibrary.org/${model}/olid/${olid}${c}`;

  return (
    <div className={classes.root}>
      {(model === 'a' || model === 'b') && (
        <img src={imgLink} alt="img" className={classes.img} />
      )}
    </div>
  );
};

ImageOLID.propTypes = {
  OLID: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
};

export default ImageOLID;
