import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';

interface TextHighlightProps {
  searchWords: string[];
  textToHighlight: string;
}

const TextHighlight: React.FC<TextHighlightProps> = props => {
  const { searchWords, textToHighlight } = props;

  return (
    <Highlighter
      highlightClassName="search-highlight"
      searchWords={searchWords}
      autoEscape
      textToHighlight={textToHighlight}
    />
    // <div />
  );
};

TextHighlight.propTypes = {
  searchWords: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  textToHighlight: PropTypes.string.isRequired,
};

export default TextHighlight;
