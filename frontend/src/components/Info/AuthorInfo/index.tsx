/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from '../styles';
import { AuthorCardAllProps } from '../types';
import getFirstThree from '../../../utils';
import PublisherName from '../../PublisherName';
import BookTitle from '../../BookTitle';
import ImageOLID from '../../ImageOLID';
import TextHighlight from '../../TextHighlight';

const AuthorInfo: React.FC<AuthorCardAllProps> = props => {
  const {
    id,
    name,
    bio,
    top_work,
    birth_date,
    death_date,
    num_works,
    website_link,
    OLID,
    books_list,
    publisherID,
    searchQuery,
  } = props;
  const classes = useStyles();

  const searchWords = searchQuery ? [searchQuery] : [];

  return (
    <div>
      <Paper className={classes.mainContainer}>
        <ImageOLID OLID={OLID} model="a" />
        <div className={classes.textContainer}>
          <Typography variant="h2" className={classes.text}>
            <TextHighlight searchWords={searchWords} textToHighlight={name} />
          </Typography>
          <div className={classes.tinyContainer}>
            <div>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Birthday: ${birth_date}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Deathday: ${death_date}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Number of works: ${num_works}`}
                />
              </Typography>
            </div>
            <div className={classes.horizontalOffset}>
              <a href={website_link} className={classes.link}>
                <Typography variant="body1" className={classes.text}>
                  <TextHighlight
                    searchWords={searchWords}
                    textToHighlight={website_link}
                  />
                </Typography>
              </a>
              <Typography
                variant="body2"
                color="textSecondary"
                component="p"
                className={classes.text}
              >
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Notable Work: ${top_work}`}
                />
              </Typography>
              <div className={classes.text}>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.infoLink}
                >
                  Works:
                  {getFirstThree(books_list).map((givenID: number) => {
                    return (
                      <BookTitle bookID={givenID} searchWords={searchWords} />
                    );
                  })}
                </Typography>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.infoLink}
                >
                  Publishers:
                  {getFirstThree(publisherID).map((givenID: number) => {
                    return (
                      <PublisherName
                        publisherID={givenID}
                        searchWords={searchWords}
                      />
                    );
                  })}
                </Typography>
              </div>
            </div>
          </div>
          <div className={classes.verticalOffset}>
            <Typography variant="body1" className={classes.text}>
              <TextHighlight searchWords={searchWords} textToHighlight={bio} />
            </Typography>
          </div>
        </div>
      </Paper>
    </div>
  );
};

AuthorInfo.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  top_work: PropTypes.string.isRequired,
  birth_date: PropTypes.string.isRequired,
  death_date: PropTypes.string.isRequired,
  num_works: PropTypes.number.isRequired,
  website_link: PropTypes.string.isRequired,
  OLID: PropTypes.string.isRequired,
  books_list: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  publisherID: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default AuthorInfo;
