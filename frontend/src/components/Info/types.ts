import { AuthorCardProps } from '../AuthorCard/types';

import { PublisherCardProps } from '../PublisherCard/types';

import { WorkCardProps } from '../WorkCard/types';

export type AuthorCardAllProps = AuthorCardProps;

export type PublisherCardAllProps = PublisherCardProps;

export type WorkCardAllProps = WorkCardProps;
