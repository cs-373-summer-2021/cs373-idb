import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {},
    mainContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',

      backgroundColor: theme.customPalette.common.greenBlue,

      width: '98%',
      minHeight: 400,

      padding: theme.spacing(1),
      margin: theme.spacing(1),
    },
    img: {
      width: '40%',
      maxHeight: 600,
      objectFit: 'contain',
    },
    textContainer: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
    },
    text: {
      color: 'white',
      marginTop: theme.spacing(0.2),
    },
    tinyContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',

      marginTop: theme.spacing(2),
    },
    horizontalOffset: {
      marginLeft: theme.spacing(4),
    },
    verticalOffset: {
      marginTop: theme.spacing(2),
    },
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
    infoLink: {
      color: 'white',
      marginTop: theme.spacing(0.2),
      textDecoration: 'none',
    },
  });
});

export default useStyles;
