/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from '../styles';
import { PublisherCardAllProps } from '../types';
import getFirstThree from '../../../utils';
import { urls } from '../constants';
import { api } from '../../../services';
import AuthorName from '../../AuthorName';
import BookTitle from '../../BookTitle';
import TextHighlight from '../../TextHighlight';

const PublisherInfo: React.FC<PublisherCardAllProps> = props => {
  const {
    id,
    name,
    status,
    founded,
    country,
    headquarters,
    website,
    books,
    authors,
    image_link,
    searchQuery,
  } = props;
  const classes = useStyles();
  const [data, setData] = useState<PublisherCardAllProps>({
    id: 0,
    name: 'default',
    status: 'default',
    founded: 'default',
    country: 'default',
    headquarters: 'default',
    website: 'default',
    books: [],
    authors: [],
    image_link: '',
    searchQuery: '',
  });

  const searchWords = searchQuery ? [searchQuery] : [];

  // const [isLoaded, setIsLoaded] = useState(true);
  const isLoaded = headquarters !== 'N/A';

  useEffect(() => {
    const path = `${urls.publisherId}${id}`;
    api
      .get(path)
      .then(value => {
        // console.log(value);
        // if (value.data.headquarters !== 'N/A') setIsLoaded(true);
        // console.log(value.data.headquarters);
        setData(value.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const mapsApiKey = 'AIzaSyBVSmS0bcTL-HM4c6w16OZBAc-pucragOY'; // TODO: Deactivate All API keys once project is complete.
  const map_url = `https://www.google.com/maps/embed/v1/place?key=${mapsApiKey}&q=${headquarters}`;

  return (
    <div>
      <Paper className={classes.mainContainer}>
        <img src={image_link} alt="img" className={classes.img} />
        <div className={classes.textContainer}>
          <Typography variant="h2" className={classes.text}>
            <TextHighlight searchWords={searchWords} textToHighlight={name} />
          </Typography>
          <div className={classes.tinyContainer}>
            {isLoaded && (
              <iframe
                title={`${headquarters} map`}
                src={map_url}
                width="80%"
                height="80%"
                loading="lazy"
              />
            )}
            <div className={classes.horizontalOffset}>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Founded: ${founded}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Status: ${status}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Country: ${country}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Headquarters: ${headquarters}`}
                />
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                component="p"
                className={classes.infoLink}
              >
                Books:
                {getFirstThree(books).map((givenID: number) => {
                  return (
                    <BookTitle bookID={givenID} searchWords={searchWords} />
                  );
                })}
              </Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                component="p"
                className={classes.infoLink}
              >
                Authors:
                {getFirstThree(authors).map((givenID: number) => {
                  return (
                    <AuthorName authorID={givenID} searchWords={searchWords} />
                  );
                })}
              </Typography>
            </div>
          </div>
          <div className={classes.verticalOffset}>
            <a href={website} className={classes.link}>
              <Typography variant="body1" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={website}
                />
              </Typography>
            </a>
          </div>
        </div>
      </Paper>
    </div>
  );
};

PublisherInfo.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  founded: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  headquarters: PropTypes.string.isRequired,
  website: PropTypes.string.isRequired,
  books: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  authors: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  image_link: PropTypes.string.isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default PublisherInfo;
