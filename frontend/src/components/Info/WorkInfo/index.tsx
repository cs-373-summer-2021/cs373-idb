/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from '../styles';
import { WorkCardAllProps } from '../types';
import getFirstThree from '../../../utils';
import { urls } from '../../../pages/InfoPages/constants';
import { api } from '../../../services';
import PublisherName from '../../PublisherName';
import AuthorName from '../../AuthorName';
import ImageOLID from '../../ImageOLID';
import TextHighlight from '../../TextHighlight';

const WorkInfo: React.FC<WorkCardAllProps> = props => {
  const {
    id,
    title,
    subtitle,
    page_count,
    description,
    isbn,
    publish_date,
    OLID,
    subjects,
    authors,
    publisher_id,
    searchQuery,
  } = props;
  const classes = useStyles();

  const searchWords = searchQuery ? [searchQuery] : [];
  const theSubtitle = subtitle || 'N/A';

  return (
    <div className={classes.root}>
      <Paper className={classes.mainContainer}>
        <ImageOLID OLID={OLID} model="b" />
        <div className={classes.textContainer}>
          <Typography variant="h2" className={classes.text}>
            <TextHighlight searchWords={searchWords} textToHighlight={title} />
          </Typography>
          <Typography variant="h3" className={classes.text}>
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={theSubtitle}
            />
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Authors:
            {getFirstThree(authors).map((givenID: number) => {
              return (
                <AuthorName authorID={givenID} searchWords={searchWords} />
              );
            })}
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Publisher:
            <PublisherName
              publisherID={publisher_id}
              searchWords={searchWords}
            />
          </Typography>
          <div className={classes.tinyContainer}>
            <div>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`isbn: ${isbn}`}
                />
              </Typography>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Publish Date: ${publish_date}`}
                />
              </Typography>
            </div>
            <div className={classes.horizontalOffset}>
              <Typography variant="body2" className={classes.text}>
                <TextHighlight
                  searchWords={searchWords}
                  textToHighlight={`Pages: ${page_count}`}
                />
              </Typography>
              {/* <Typography variant="body2" className={classes.text}>
                {`Categories: ${subjects}`}
              </Typography> */}
            </div>
          </div>
          <div className={classes.verticalOffset}>
            <Typography variant="body1" className={classes.text}>
              <TextHighlight
                searchWords={searchWords}
                textToHighlight={description}
              />
            </Typography>
          </div>
        </div>
      </Paper>
    </div>
  );
};

WorkInfo.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  page_count: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  isbn: PropTypes.string.isRequired,
  publish_date: PropTypes.string.isRequired,
  OLID: PropTypes.string.isRequired,
  subjects: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  authors: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  publisher_id: PropTypes.number.isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default WorkInfo;
