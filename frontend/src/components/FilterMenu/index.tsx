import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { MenuList } from '@material-ui/core';
import { FilterMenuProps } from './types';
import useStyles from './styles';

const FilterMenu: React.FC<FilterMenuProps> = props => {
  const {
    filterOptionsByYear,
    filterOptionsByQuantity,
    filterOptionsByYesNo,
    filterOptionsByCountry,
    filterOptionsBySubject,
    filterOptionsByLetter,
    setMinDate,
    setMaxDate,
    setMinDate2,
    setMaxDate2,
    setStatus,
    setCountry,
    setWorkMin,
    setWorkMax,
    setLetter,
    filter,
    setFilter,
  } = props;
  const years = [
    1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911,
    1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923,
    1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935,
    1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947,
    1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959,
    1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971,
    1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983,
    1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995,
    1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
    2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019,
    2020, 2021,
  ];

  const quantities = [
    // eslint-disable-next-line prettier/prettier
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000,
  ];

  const yesNo = ['Yes', 'No'];

  // TODO: find a way to auto generate?
  // eslint-disable-next-line prettier/prettier
  const countries = ['Afghanistan', 'Åland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bangladesh', 'Barbados', 'Bahamas', 'Bahrain', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'British Virgin Islands', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burma', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo-Brazzaville', 'Congo-Kinshasa', 'Cook Islands', 'Costa Rica', '$_[', 'Croatia', 'Curaçao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'El Salvador', 'Egypt', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Federated States of Micronesia', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Lands', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Heard and McDonald Islands', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Islands', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Réunion', 'Romania', 'Russia', 'Rwanda', 'Saint Barthélemy', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre and Miquelon', 'Saint Vincent', 'Samoa', 'San Marino', 'São Tomé and Príncipe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia', 'South Korea', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Sweden', 'Swaziland', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City', 'Vietnam', 'Venezuela', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe'];

  // TODO: update later!
  const subjects = ['Fiction', 'NonFiction'];

  // eslint-disable-next-line prettier/prettier
  const lettersAndNumbers = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = (event: any) => {
    setOpen(true);
  };

  // when 'Filter' is pressed
  const handleFilterSelections = (event: any) => {
    setOpen(false);
    if (filter) setFilter(false);
    setFilter(true);
  };

  // when 'Cancel' is pressed
  const handleCancel = (event: any) => {
    setOpen(false);
    setFilter(false);
  };

  const handleMinDate = (option: any, section: number) => {
    // console.log(option);
    if (section === 0) setMinDate(option);
    else setMinDate2(option);
  };

  const handleMaxDate = (option: any, section: number) => {
    // console.log(option);
    if (section === 0) setMaxDate(option);
    else setMaxDate2(option);
  };

  const handleYesNo = (option: any) => {
    // console.log(option);
    if (option === 'Yes') {
      setStatus('active');
    } else {
      setStatus('inactive');
    }
  };

  const handleCountry = (option: any) => {
    // console.log(option);
    setCountry(option);
  };

  const handleQuantityMin = (option: any) => {
    // console.log(option);
    setWorkMin(option);
  };

  const handleQuantityMax = (option: any) => {
    // console.log(option);
    setWorkMax(option);
  };

  const handleLetter = (option: any) => {
    // console.log(option);
    setLetter(option);
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        Filter By
      </Button>
      <Dialog
        open={open}
        onClose={handleCancel}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Filter By</DialogTitle>
        <DialogContent>
          {filterOptionsByYear.map(function (filterOption, section) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <InputLabel>Minimum</InputLabel>
                  <Select className={classes.select} value="min">
                    <MenuList>
                      {years.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleMinDate(option, section)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
                <div className={classes.minMax}>
                  <InputLabel>Maximum</InputLabel>
                  <Select className={classes.select} value="max">
                    <MenuList>
                      {years.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleMaxDate(option, section)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
              </div>
            );
          })}
          {filterOptionsByQuantity.map(function (filterOption) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <InputLabel>Minimum</InputLabel>
                  <Select className={classes.select} value="min">
                    <MenuList>
                      {quantities.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleQuantityMin(option)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
                <div className={classes.minMax}>
                  <InputLabel>Maximum</InputLabel>
                  <Select className={classes.select} value="max">
                    <MenuList>
                      {quantities.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleQuantityMax(option)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
              </div>
            );
          })}
          {filterOptionsByYesNo.map(function (filterOption) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <Select className={classes.select} value="yesNo">
                    <MenuList>
                      {yesNo.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleYesNo(option)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
              </div>
            );
          })}
          {filterOptionsByCountry.map(function (filterOption) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <Select className={classes.select} value="yesNo">
                    <MenuList>
                      {countries.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleCountry(option)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
              </div>
            );
          })}
          {filterOptionsBySubject.map(function (filterOption) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <Select className={classes.select} value="yesNo">
                    {subjects.map(function (subject) {
                      return <MenuItem>{subject}</MenuItem>;
                    })}
                  </Select>
                </div>
              </div>
            );
          })}
          {filterOptionsByLetter.map(function (filterOption) {
            return (
              <div>
                <InputLabel>{filterOption}</InputLabel>
                <div className={classes.minMax}>
                  <Select className={classes.select} value="yesNo">
                    <MenuList>
                      {lettersAndNumbers.map(option => (
                        <MenuItem
                          key={option}
                          onClick={event => handleLetter(option)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Select>
                </div>
              </div>
            );
          })}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleFilterSelections} color="primary">
            Filter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

FilterMenu.propTypes = {
  filterOptionsByYear: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  filterOptionsByQuantity: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  filterOptionsByYesNo: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  filterOptionsByCountry: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  filterOptionsBySubject: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  filterOptionsByLetter: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
  setMinDate: PropTypes.func.isRequired,
  setMaxDate: PropTypes.func.isRequired,
  setMinDate2: PropTypes.func.isRequired,
  setMaxDate2: PropTypes.func.isRequired,
  setStatus: PropTypes.func.isRequired,
  setCountry: PropTypes.func.isRequired,
  setWorkMin: PropTypes.func.isRequired,
  setWorkMax: PropTypes.func.isRequired,
  setLetter: PropTypes.func.isRequired,
  filter: PropTypes.bool.isRequired,
  setFilter: PropTypes.func.isRequired,
};

export default FilterMenu;
