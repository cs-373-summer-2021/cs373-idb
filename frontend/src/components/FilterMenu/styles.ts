import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    minMax: {
      display: 'inline-block',
      flexDirection: 'row',
      alignItems: 'center',
      padding: '15px',
    },
    select: {
      minWidth: 100,
    },
  });
});

export default useStyles;
