export interface FilterMenuProps {
  filterOptionsByYear: string[];
  filterOptionsByQuantity: string[];
  filterOptionsByYesNo: string[];
  filterOptionsByCountry: string[];
  filterOptionsBySubject: string[];
  filterOptionsByLetter: string[];
  setMinDate: React.Dispatch<React.SetStateAction<string>>;
  setMinDate2: React.Dispatch<React.SetStateAction<string>>;
  setMaxDate: React.Dispatch<React.SetStateAction<string>>;
  setMaxDate2: React.Dispatch<React.SetStateAction<string>>;
  setStatus: React.Dispatch<React.SetStateAction<string>>;
  setCountry: React.Dispatch<React.SetStateAction<string>>;
  setWorkMin: React.Dispatch<React.SetStateAction<string>>;
  setWorkMax: React.Dispatch<React.SetStateAction<string>>;
  setLetter: React.Dispatch<React.SetStateAction<string>>;
  filter: boolean;
  setFilter: React.Dispatch<React.SetStateAction<boolean>>;
}
