import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    centerContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    horizontalOffset: {
      marginLeft: theme.spacing(2),
    },
  });
});

export default useStyles;
