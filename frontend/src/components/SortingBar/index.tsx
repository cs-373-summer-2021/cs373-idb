import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { api } from '../../services';
import { SortingBarProps } from './types';
import useStyles from './styles';

// export default function SortingBar() {
const SortingBar: React.FC<SortingBarProps> = props => {
  // const columns = ['laylah', 'house', 'is', 'a', 'woman'];
  const { columns, setOrderBy, options, setOrdering } = props;

  const classes = useStyles();

  const handleOnChange = (event: any) => {
    // console.log('event');
    // console.log(event);
    // console.log('event.target.defaultValue');
    // console.log(event.target.defaultValue);
    setOrderBy(event.target.defaultValue);
  };
  const handleOnChangeOptions = (event: any) => {
    // console.log('event');
    // console.log(event);
    // console.log('event.target.defaultValue');
    // console.log(event.target.defaultValue);
    setOrdering(event.target.defaultValue);
  };

  return (
    <div>
      <FormControl component="fieldset">
        <FormLabel component="legend">Sort By</FormLabel>
        <div className={classes.centerContainer}>
          <RadioGroup
            row
            aria-label="position"
            name="position"
            defaultValue="top"
          >
            {columns.map(function (column) {
              return (
                <FormControlLabel
                  value={column}
                  control={<Radio color="primary" />}
                  label={column}
                  labelPlacement="bottom"
                  disabled={false}
                  onChange={event => handleOnChange(event)}
                />
              );
            })}
          </RadioGroup>
          <RadioGroup
            row
            aria-label="position"
            name="position"
            defaultValue="top"
            className={classes.horizontalOffset}
          >
            {options.map(function (column) {
              return (
                <FormControlLabel
                  value={column}
                  control={<Radio color="primary" />}
                  label={column}
                  labelPlacement="bottom"
                  disabled={false}
                  onChange={event => handleOnChangeOptions(event)}
                />
              );
            })}
          </RadioGroup>
        </div>
      </FormControl>
    </div>
  );
};

SortingBar.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  setOrderBy: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  setOrdering: PropTypes.func.isRequired,
};

export default SortingBar;
