export interface SortingBarProps {
  columns: string[];
  setOrderBy: React.Dispatch<React.SetStateAction<string>>;
  options: string[];
  setOrdering: React.Dispatch<React.SetStateAction<string>>;
}
