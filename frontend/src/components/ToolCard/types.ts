export interface ToolCardProps {
  name: string;
  description: string;
  photo: string;
}
