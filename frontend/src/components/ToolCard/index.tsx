import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles';
import { ToolCardProps } from './types';

const ToolCard: React.FC<ToolCardProps> = props => {
  const classes = useStyles();

  // -- card information --
  const { name, description, photo } = props;

  return (
    <Card className={classes.root}>
      <CardMedia className={classes.media} image={photo} title="Image" />
      <CardHeader title={name} />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};

ToolCard.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
};

export default ToolCard;
