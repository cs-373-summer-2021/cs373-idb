import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 200,
      // maxWidth: 345,
      width: 200,
      minHeight: 360,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    media: {
      width: 200,
      height: 200,
      objectFit: 'contain',
    },
  });
});

export default useStyles;
