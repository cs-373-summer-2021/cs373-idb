/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import Collapse from '@material-ui/core/Collapse';
import useStyles from './styles';
import { WorkCardProps } from './types';
import getFirstThree from '../../utils';
import { AuthorCardAllProps, PublisherCardAllProps } from '../Info/types';
import { urls } from '../../pages/InfoPages/constants';
import { api } from '../../services';
import PublisherName from '../PublisherName';
import AuthorName from '../AuthorName';
import ImageOLID from '../ImageOLID';
import TextHighlight from '../TextHighlight';

const WorkCard: React.FC<WorkCardProps> = props => {
  const classes = useStyles();

  // -- card information --
  const {
    id,
    title,
    subtitle,
    page_count,
    description,
    isbn,
    publish_date,
    OLID,
    subjects,
    authors,
    publisher_id,
    searchQuery,
  } = props;

  const searchWords = searchQuery ? [searchQuery] : [];
  const theSubtitle = subtitle || 'N/A';

  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <Link to={`/works/${id}`} className={classes.infoLink}>
        <ImageOLID OLID={OLID} model="b" />
        <Typography
          variant="h5"
          color="textPrimary"
          component="p"
          align="center"
        >
          <TextHighlight searchWords={searchWords} textToHighlight={title} />
        </Typography>
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Subtitle: ${theSubtitle}`}
            />
          </Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.infoLink}
          >
            Authors:
            {getFirstThree(authors).map((givenID: number) => {
              return (
                <AuthorName authorID={givenID} searchWords={searchWords} />
              );
            })}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Publisher:
            <PublisherName
              publisherID={publisher_id}
              searchWords={searchWords}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Page count: ${page_count}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`isbn: ${isbn}`}
            />
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`Publish Date: ${publish_date}`}
            />
          </Typography>
        </CardContent>
      </Link>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            // noWrap
            className="biography"
          >
            <TextHighlight
              searchWords={searchWords}
              textToHighlight={`${description}`}
            />
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};

WorkCard.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  page_count: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  isbn: PropTypes.string.isRequired,
  publish_date: PropTypes.string.isRequired,
  OLID: PropTypes.string.isRequired,
  subjects: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  authors: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  publisher_id: PropTypes.number.isRequired,
  searchQuery: PropTypes.string.isRequired,
};

export default WorkCard;
