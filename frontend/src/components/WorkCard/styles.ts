import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // minWidth: 300,
      // maxWidth: 300,
      minHeight: 400,
      width: 300,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'right',
      overflow: 'hidden',
    },
    media: {
      width: 300,
      height: 300,
      objectFit: 'contain',
    },
    infoLink: {
      textDecoration: 'none',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    biography: {},
  });
});

export default useStyles;
