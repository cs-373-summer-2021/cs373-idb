export const models = {
  author: 'work',
  publisher: 'publisher',
  work: 'work',
};

export const urls = {
  authorId: 'api/author/',
  publisherId: 'api/publisher/',
  workId: 'api/work/',
};
