import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useStyles from '../styles';
import { InfoPageProps } from '../types';
import { urls } from '../constants';
import { api } from '../../../services';
import PublisherInfo from '../../../components/Info/PublisherInfo';
import { PublisherCardAllProps } from '../../../components/Info/types';

const PublisherInfoPage: React.FC<InfoPageProps> = props => {
  const { id } = props;
  const classes = useStyles();

  const [data, setData] = useState<PublisherCardAllProps>({
    id: 0,
    name: 'default',
    status: 'default',
    founded: 'default',
    country: 'default',
    headquarters: 'default',
    website: 'default',
    books: [],
    authors: [],
    image_link: '',
    searchQuery: '',
  });

  useEffect(() => {
    const path = `${urls.publisherId}${id}`;
    api
      .get(path)
      .then(value => {
        // console.log(value);
        setData(value.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div className={classes.root}>
      {data && (
        <PublisherInfo
          id={data.id}
          name={data.name}
          founded={data.founded}
          status={data.status}
          country={data.country}
          headquarters={data.headquarters}
          website={data.website}
          books={data.books}
          authors={data.authors}
          image_link={data.image_link}
          searchQuery={data.searchQuery}
        />
      )}
    </div>
  );
};

PublisherInfoPage.propTypes = {
  id: PropTypes.number.isRequired,
};

export default PublisherInfoPage;
