import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useStyles from '../styles';
import { InfoPageProps } from '../types';
import { urls } from '../constants';
import { api } from '../../../services';
import {
  AuthorCardAllProps,
  PublisherCardAllProps,
  WorkCardAllProps,
} from '../../../components/Info/types';
import WorkInfo from '../../../components/Info/WorkInfo';

const WorkInfoPage: React.FC<InfoPageProps> = props => {
  const { id } = props;
  const classes = useStyles();

  const [data, setData] = useState<WorkCardAllProps>({
    id: 0,
    title: 'default',
    subtitle: null,
    page_count: 0,
    description: 'default',
    isbn: 'default',
    publish_date: 'default',
    OLID: 'default',
    subjects: [],
    authors: [],
    publisher_id: 0,
    searchQuery: '',
  });

  useEffect(() => {
    const path = `${urls.workId}${id}`;
    api
      .get(path)
      .then(value => {
        // console.log(value);
        setData(value.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div className={classes.root}>
      {data && data.publisher_id && (
        <WorkInfo
          id={data.id}
          title={data.title}
          subtitle={data.subtitle}
          page_count={data.page_count}
          description={data.description}
          isbn={data.isbn}
          publish_date={data.publish_date}
          OLID={data.OLID}
          subjects={data.subjects}
          authors={data.authors}
          publisher_id={data.publisher_id}
          searchQuery={data.searchQuery}
        />
      )}
    </div>
  );
};

WorkInfoPage.propTypes = {
  id: PropTypes.number.isRequired,
};

export default WorkInfoPage;
