import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useStyles from '../styles';
import { InfoPageProps } from '../types';
import { urls } from '../constants';
import { api } from '../../../services';
import AuthorInfo from '../../../components/Info/AuthorInfo';
import { AuthorCardAllProps } from '../../../components/Info/types';

const AuthorInfoPage: React.FC<InfoPageProps> = props => {
  const { id } = props;
  const classes = useStyles();

  const [data, setData] = useState<AuthorCardAllProps>({
    id: 0,
    name: 'default',
    bio: 'default',
    top_work: 'default',
    birth_date: 'default',
    death_date: 'default',
    num_works: 0,
    website_link: 'default',
    OLID: 'default',
    books_list: [],
    publisherID: [],
    searchQuery: '',
  });

  useEffect(() => {
    const path = `${urls.authorId}${id}`;
    api
      .get(path)
      .then(value => {
        // console.log(value);
        setData(value.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div className={classes.root}>
      {data && (
        <AuthorInfo
          id={data.id}
          name={data.name}
          bio={data.bio}
          top_work={data.top_work}
          birth_date={data.birth_date}
          death_date={data.death_date}
          num_works={data.num_works}
          website_link={data.website_link}
          OLID={data.OLID}
          books_list={data.books_list}
          publisherID={data.publisherID}
          searchQuery={data.searchQuery}
        />
      )}
    </div>
  );
};

AuthorInfoPage.propTypes = {
  id: PropTypes.number.isRequired,
};

export default AuthorInfoPage;
