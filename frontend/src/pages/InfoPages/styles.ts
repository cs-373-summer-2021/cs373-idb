import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import libraryDark from '../../assets/library-dark.jpg';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',

      backgroundImage: `url(${libraryDark})`,
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',

      minHeight: '100vh',
    },
    mainContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',

      backgroundColor: theme.customPalette.common.greenBlue,

      width: '98%',
      minHeight: 400,

      padding: theme.spacing(1),
      margin: theme.spacing(1),
    },
    img: {
      width: '40%',
      maxHeight: 600,
      objectFit: 'contain',
    },
    textContainer: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
    },
    text: {
      color: 'white',
      marginTop: theme.spacing(0.2),
    },
    tinyContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',

      marginTop: theme.spacing(2),
    },
    horizontalOffset: {
      marginLeft: theme.spacing(4),
    },
    verticalOffset: {
      marginTop: theme.spacing(2),
    },
    link: {
      // textDecoration: 'none',
      color: theme.customPalette.common.lightPurple,
    },
  });
});

export default useStyles;
