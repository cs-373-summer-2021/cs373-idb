import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      minHeight: '100vh',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      // justifyContent: 'center',
    },
    title: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(1),
    },
    bar: {
      width: '98%',
    },
  });
});

export default useStyles;
