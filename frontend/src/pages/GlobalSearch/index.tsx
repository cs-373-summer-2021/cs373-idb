/* eslint-disable camelcase */
import { Typography } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import PublisherSearch from '../../components/Search/Publisher';
import useStyles from './styles';
import AuthorSearch from '../../components/Search/Author';
import WorkSearch from '../../components/Search/Work';

export interface GlobalSearchProps {
  searchQuery: string;
}

const GlobalSearch: React.FC<GlobalSearchProps> = props => {
  const { searchQuery } = props;

  const classes = useStyles();

  return (
    <>
      {searchQuery ? (
        <div className={classes.root}>
          <Typography variant="h5" className={classes.title}>
            {`Search Results for: ${searchQuery}`}
          </Typography>
          <hr className={classes.bar} />
          <PublisherSearch searchQuery={searchQuery} />
          <hr className={classes.bar} />
          <AuthorSearch searchQuery={searchQuery} />
          <hr className={classes.bar} />
          <WorkSearch searchQuery={searchQuery} />
        </div>
      ) : (
        <div className={classes.root}>
          <Typography variant="h5" className={classes.title}>
            Empty Search!
          </Typography>
        </div>
      )}
    </>
  );
};

GlobalSearch.propTypes = {
  searchQuery: PropTypes.string.isRequired,
};

export default GlobalSearch;
