import React, { useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import WorkCard from '../../components/WorkCard';
import { WorkCardProps } from '../../components/WorkCard/types';
import useStyles from './styles';
import { api } from '../../services';
import SortingBar from '../../components/SortingBar';
import FilterMenu from '../../components/FilterMenu';
import Progress from '../../components/Progress';
import Pagination from '../../components/Pagination';
import SearchBar from '../../components/SearchBar';

const Works: React.FC = () => {
  const classes = useStyles();
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(15);
  const [count, setCount] = useState(10);
  const [isLoading, setIsLoading] = useState(true);
  const columns = [
    'Title (A-Z)',
    'Subtitle',
    'Page Count',
    'ISBN',
    'Date of Publication',
  ];
  // -- filter stuff --
  const filterOptionsByQuantity = ['Number of Pages'];
  const filterOptionsByYear = ['Year Published'];
  const filterOptionsBySubject = ['Subject'];
  const filterOptionsByLetter = ['Starts With'];
  const [minDate, setMinDate] = useState('1900');
  const [maxDate, setMaxDate] = useState('2021');
  const [minDate2, setMinDate2] = useState('1900');
  const [maxDate2, setMaxDate2] = useState('2021');
  const [workMin, setWorkMin] = useState('0');
  const [workMax, setWorkMax] = useState('1000');
  const [letter, setLetter] = useState('');
  const [status, setStatus] = useState('active');
  const [country, setCountry] = useState('');
  const [filter, setFilter] = useState(false);
  // -- sorting stuff --
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc
  const [data, setData] = useState([]);

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'title';
    }
    if (givenOrderBy === columns[1]) {
      return 'subtitle';
    }
    if (givenOrderBy === columns[2]) {
      return 'page_count';
    }
    if (givenOrderBy === columns[3]) {
      return 'isbn';
    }
    return 'publish_date';
  };
  const [q, setQuery] = useState('');

  const getRange = (min: string, max: string) => {
    return `${min}-${max}`;
  };

  useEffect(() => {
    const path = 'api/work/all/sorted';
    const params = filter
      ? {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
          page_range: getRange(workMin, workMax),
          published_range: getRange(minDate, maxDate),
          starts_with: letter,
        }
      : {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
        };
    console.log(params);
    api
      // .get(path, {params: { orderBy: getOrderBy(orderBy), ordering, q },
      .get(path, {
        params,
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })
      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, q, page, rowsPerPage, filter]);

  return (
    <div className={classes.root}>
      <h1>Works</h1>
      <div className={classes.container}>
        <SortingBar
          columns={columns}
          setOrderBy={setOrderBy}
          options={options}
          setOrdering={setOrdering}
        />
        <div className={classes.filterMenu}>
          <FilterMenu
            filterOptionsByYear={filterOptionsByYear}
            filterOptionsByQuantity={filterOptionsByQuantity}
            filterOptionsByYesNo={[]}
            filterOptionsByCountry={[]}
            filterOptionsBySubject={[]}
            filterOptionsByLetter={filterOptionsByLetter}
            setWorkMin={setWorkMin}
            setWorkMax={setWorkMax}
            setMinDate={setMinDate}
            setMinDate2={setMinDate2}
            setMaxDate={setMaxDate}
            setMaxDate2={setMaxDate2}
            setStatus={setStatus}
            setCountry={setCountry}
            filter={filter}
            setFilter={setFilter}
            setLetter={setLetter}
          />
        </div>
      </div>
      <div className={classes.space}>
        <div className={classes.search_root}>
          <SearchBar setQuery={setQuery} />
        </div>
      </div>
      {isLoading ? (
        <Progress />
      ) : (
        <>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="flex-start"
            className={classes.gridContainer}
          >
            {data &&
              data.map((work: WorkCardProps) => {
                return (
                  <Grid
                    item
                    xs={4}
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                    zeroMinWidth
                  >
                    <WorkCard
                      id={work.id}
                      title={work.title}
                      subtitle={work.subtitle}
                      page_count={work.page_count}
                      description={work.description}
                      isbn={work.isbn}
                      publish_date={work.publish_date}
                      OLID={work.OLID}
                      subjects={work.subjects}
                      authors={work.authors}
                      publisher_id={work.publisher_id}
                      searchQuery={q}
                    />
                  </Grid>
                );
              })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Books per page"
          />
        </>
      )}
    </div>
  );
};

export default Works;
