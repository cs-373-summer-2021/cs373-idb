import React from 'react';
import { Button, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

import useStyles from './styles';

const Homepage: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.workContainer}>
        <div className={classes.textContainerLeft}>
          <Typography variant="h3" className={classes.mainText}>
            Learn more or search for books.
          </Typography>
          <Link to="/works" className={classes.link}>
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Books
            </Button>
          </Link>
        </div>
      </div>
      <div className={classes.authorContainer}>
        <div className={classes.textContainerRight}>
          <Typography variant="h3" className={classes.mainText}>
            Learn more or search for an author.
          </Typography>
          <Link to="/authors" className={classes.link}>
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Authors
            </Button>
          </Link>
        </div>
      </div>
      <div className={classes.publisherContainer}>
        <div className={classes.textContainerLeft}>
          <Typography variant="h3" className={classes.mainText}>
            Learn more or search for a publisher.
          </Typography>
          <Link to="/publishers" className={classes.link}>
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Publishers
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Homepage;
