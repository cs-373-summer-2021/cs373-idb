import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import libraryTunnel from '../../assets/library-tunnel.jpg';
import libraryGlobe from '../../assets/library-globe.jpg';
import libraryComfy from '../../assets/library-comfy.jpg';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      // display: 'flex',
      // flexDirection: 'column',
      // alignItems: 'center',

      minHeight: '100vh',

      marginTop: theme.spacing(1),
    },
    button: {
      fontSize: 24,

      marginTop: theme.spacing(2),
      marginRight: theme.spacing(0.5),
      marginLeft: theme.spacing(0.5),
      padding: theme.spacing(1),
    },
    textContainerRight: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
    textContainerLeft: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
    mainText: {
      // textAlign: 'center',
      color: 'white',
      opacity: '90%',
    },
    workContainer: {
      backgroundImage: `url(${libraryTunnel})`,
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',

      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',

      height: 400,
      width: '100%',
    },
    authorContainer: {
      backgroundImage: `url(${libraryGlobe})`,
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',

      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',

      height: 400,
      width: '100%',
    },
    publisherContainer: {
      backgroundImage: `url(${libraryComfy})`,
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',

      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',

      height: 400,
      width: '100%',
    },
    link: {
      textDecoration: 'none',
    },
  });
});

export default useStyles;
