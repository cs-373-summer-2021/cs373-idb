export interface UnitTestResults {
  errors: number;
  failures: number;
}
