import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      minHeight: '100vh',
      marginTop: theme.spacing(2),
    },
    media: {
      width: 300,
      height: 'auto',
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    gridContainer: {
      paddingLeft: theme.spacing(20),
      paddingRight: theme.spacing(20),
      paddingBottom: theme.spacing(3),
    },
    main: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(4),
    },
    link: {
      textDecoration: 'none',
    },
    input: {
      display: 'none',
    },
    inputRoot: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    centerContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    scrapingText: {
      width: '80%',
    },
  });
});

export default useStyles;
