/* eslint-disable no-await-in-loop */
import React, { useEffect, useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { api } from '../../services';
import useStyles from './styles';
import AboutCard from '../../components/AboutCard';
import { AboutCardProps } from '../../components/AboutCard/types';
import templateImg from '../../assets/logo512.png';
import laylahHouseImg from '../../assets/laylahHeadshot.png';
import avinashImg from '../../assets/headshot.jpeg';
import erikImg from '../../assets/erikHeadshot.png';
import vanImg from '../../assets/vandanaHeadshot.jpg';
import viswaImg from '../../assets/viswaHeadshot.jpg';
import ToolCard from '../../components/ToolCard';
import Progress from '../../components/Progress';

const About: React.FC = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const commitsUrl =
    'https://gitlab.com/api/v4/projects/27572811/repository/commits';
  const issuesUrl = 'https://gitlab.com/api/v4/projects/27572811/issues';
  const [memberCommits, setMemberCommits] = useState([0, 0, 0, 0, 0]);
  const [memberIssues, setMemberIssues] = useState([0, 0, 0, 0, 0]);
  const [commitTotals, setCommitTotals] = useState(0);
  const [issueTotals, setIssueTotals] = useState(0);

  /* Got this Function from Emaan Haseem's project BreathEasy - file: About.jsx
  url: https://gitlab.com/joeytgraham/fitness-god/
  */
  /* get commits from gitlab */
  useEffect(() => {
    const getCommits = async () => {
      const stats = [0, 0, 0, 0, 0, 0];
      /* to handle paginated response from gitlab */
      let xNextPage: string | null = '1';
      while (xNextPage) {
        const response = await fetch(`${commitsUrl}?page=${xNextPage}`);
        const OUR_COMMITS = await response.json();
        OUR_COMMITS.forEach(commit => {
          const name = commit.author_email;
          /* update individual commit numbers */
          if (name === 'erik.bari@mac.com') {
            stats[0] += 1;
          } else if (name === 'vgeorge@utexas.edu') {
            stats[1] += 1;
          } else if (name === 'viswanathkasireddy@gmail.com') {
            stats[2] += 1;
          } else if (name === 'jimminash@gmail.com') {
            stats[3] += 1;
          } else if (name === 'laylah.house@utexas.edu') {
            stats[4] += 1;
          }
          stats[5] += 1;
        });
        xNextPage = response.headers.get('x-next-page');
      }
      setMemberCommits(stats);
      setCommitTotals(stats[5]);
    };
    getCommits();
  });

  /* get issues from gitlab */
  useEffect(() => {
    const getIssues = async () => {
      const issues = [0, 0, 0, 0, 0, 0];
      /* to handle paginated response from gitlab */
      let xNextPage: string | null = '1';
      while (xNextPage) {
        const response = await fetch(`${issuesUrl}?page=${xNextPage}`);
        const OUR_ISSUES = await response.json();
        OUR_ISSUES.forEach(issue => {
          const { assignees } = issue;
          assignees.forEach(assignee => {
            const { name } = assignee;
            /* update individual issues numbers */
            if (name === 'Erik Bari Dos Reis') {
              issues[0] += 1;
            } else if (name === 'Vandana L George') {
              issues[1] += 1;
            } else if (name === 'Viswanath R Kasireddy') {
              issues[2] += 1;
            } else if (name === 'Avinash Iyer') {
              issues[3] += 1;
            } else if (name === 'Laylah House') {
              issues[4] += 1;
            }
            issues[5] += 1;
          });
        });
        xNextPage = response.headers.get('x-next-page');
      }
      setMemberIssues(issues);
      setIssueTotals(issues[5]);
    };
    getIssues();
    console.log(memberCommits);
    console.log(memberIssues);
  }, []);

  const [isLoading, setIsLoading] = useState(false);

  const handleClickUnitTest = () => {
    setIsLoading(true);
    const path = '/test/unittest';
    api
      .get(path)
      .then(value => {
        // console.log(value);
        setIsLoading(false);
        setData(value.data);
        alert(
          `Unit Test: failures: ${value.data.failures}, errors: ${value.data.errors}`,
        );
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <div className={classes.root}>
      <h1>Meet the BookSleuth Team</h1>
      <Grid
        container
        direction="row"
        justify="center"
        spacing={6}
        alignItems="flex-start"
        className={classes.gridContainer}
      >
        <Grid
          item
          xs={4}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <AboutCard
            name="Erik Bari Dos Reis"
            job="Team Leader, FullStack Developer"
            bio="Erik Bari Dos Reis is a senior at the University of Texas at Austin and a team leader. He has been working at internships since the beginning of 2021, learning web development and software engineering skills. Currently working towards a bachelors of science in computer science. Erik enjoys learning, reading, hiking, and playing video games when time permits."
            responsibilities="FullStack, Deployment, Version Control, Virtualization"
            commits={memberCommits[0]}
            issues={memberIssues[0]}
            unitTests={34}
            photo={erikImg}
          />
        </Grid>
        <Grid
          item
          xs={4}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <AboutCard
            name="Laylah House"
            job="Developer"
            bio="Laylah House is a highly motivated and dedicated senior at the University of
            Texas at Austin who intends to improve and inspire the world around her through
            computers and technology. In her free time, Laylah enjoys dancing salsa and bachata,
            shopping, and trying new restaurants around Austin."
            responsibilities="Front End, Unit Testing, Deployment"
            commits={memberCommits[4]}
            issues={memberIssues[4]}
            unitTests={14}
            photo={laylahHouseImg}
          />
        </Grid>
        <Grid
          item
          xs={4}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <AboutCard
            name="Vandana George"
            job="Developer"
            bio="Vandana George is a senior at the University of Texas at Austin, aiming to earn a BS in Computer Science.
            She is especially interested in frontend development as well as machine learning. When she is not working, she
            enjoys painting, drawing, and baking."
            responsibilities="Front End, Virtualization"
            commits={memberCommits[1]}
            issues={memberIssues[1]}
            unitTests={14}
            photo={vanImg}
          />
        </Grid>
        <Grid
          item
          xs={6}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <AboutCard
            name="Avinash Iyer"
            job="Developer"
            bio="Avinash Iyer is a Senior at the Univesity of Texas at Austin. He is currently pursuing a
            degree in Computer Science. He has a strong interest in database programming and algorithmic design.
            Additionally, he enjoys playing video games in his free time."
            responsibilities="Backend, Databases"
            commits={memberCommits[3]}
            issues={memberIssues[3]}
            unitTests={14}
            photo={avinashImg}
          />
        </Grid>
        <Grid
          item
          xs={6}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <AboutCard
            name="Viswa Kasireddy"
            job="Developer"
            bio="Senior in Computer Science. Interested in AI, Machine Learning, and Sports Analytics (Basketball)."
            responsibilities="Backend, Databases"
            commits={memberCommits[2]}
            issues={memberIssues[2]}
            unitTests={14}
            photo={viswaImg}
          />
        </Grid>
      </Grid>
      <div>
        <Typography variant="h5">
          Total Stats: commits: 59 issues: 14 unit tests: 34
        </Typography>
      </div>
      <div className={classes.inputRoot}>
        <div className={classes.centerContainer}>
          <Button
            variant="contained"
            color="primary"
            component="span"
            size="large"
            onClick={handleClickUnitTest}
          >
            RUN UNIT TESTS
          </Button>
          {isLoading && <Progress />}
        </div>
      </div>
      <h2> Scraping </h2>
      <Typography
        paragraph
        variant="body1"
        color="textPrimary"
        component="p"
        className={classes.scrapingText}
      >
        {`Through the use of Python's requests library (which allows the user to
        abstract the complexities of making HTTP requests), we accessed the
        Wikimedia REST API and OpenLibrary RESTful API. We opted to store the
        results of our requests as strings, parse through them in Python to
        extract the relevant information, then store it in our database through
        the use of SQLAlchemy.`}
      </Typography>
      <h2>APIs and Data Sources </h2>
      <div className={classes.main}>
        <a
          href="https://openlibrary.org/developers/api"
          className={classes.link}
        >
          <ToolCard
            name="OpenLibrary RESTful API"
            description="Information on books, publishers, authors"
            photo="https://pbs.twimg.com/profile_images/803722648198881280/5hV-C1mN.jpg"
          />
        </a>
        <a
          href="https://www.mediawiki.org/wiki/API:Main_page"
          className={classes.link}
        >
          <ToolCard
            name="Wikimedia REST API"
            description="Information on books, publishers, authors"
            photo="https://upload.wikimedia.org/wikipedia/commons/8/81/Wikimedia-logo.svg"
          />
        </a>
      </div>
      <h2>Tools</h2>
      <div className={classes.main}>
        <a href="https://code.visualstudio.com/" className={classes.link}>
          <ToolCard
            name="VSCode"
            description="IDE used for coding our project"
            photo="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1200px-Visual_Studio_Code_1.35_icon.svg.png"
          />
        </a>
        <a href="https://reactjs.org/" className={classes.link}>
          <ToolCard
            name="React"
            description="React typescript template used for front-end development"
            photo="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png"
          />
        </a>
        <a
          href="https://gitlab.com/cs-373-summer-2021/cs373-idb/-/tree/main"
          className={classes.link}
        >
          <ToolCard
            name="Gitlab"
            description="Git-repository manager used for issue tracking & Pipelining"
            photo="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png"
          />
        </a>
      </div>
      <div className={classes.main}>
        <a href="https://www.docker.com/" className={classes.link}>
          <ToolCard
            name="Docker"
            description="Platform used for virtualization & consistent environments"
            photo="https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/vertical-logo-monochromatic.png?itok=erja9lKc"
          />
        </a>
        <a href="https://material-ui.com/" className={classes.link}>
          <ToolCard
            name="MaterialUI"
            description="React UI framework used for web development"
            photo="https://material-ui.com/static/logo_raw.svg"
          />
        </a>
        <a href="https://discord.com/" className={classes.link}>
          <ToolCard
            name="Discord"
            description="Platform used for collaboration"
            photo="https://discord.com/assets/3437c10597c1526c3dbd98c737c2bcae.svg"
          />
        </a>
        <a
          href="https://documenter.getpostman.com/view/16349010/Tzedf46U"
          className={classes.link}
        >
          <ToolCard
            name="Postman API"
            description="Testing endpoints of APIs"
            photo="https://uploads.sitepoint.com/wp-content/uploads/2017/08/1502958734postman.png"
          />
        </a>
      </div>
      <h2>Additional Links</h2>
      <div className={classes.main}>
        <a
          href="https://gitlab.com/cs-373-summer-2021/cs373-idb/-/issues"
          className={classes.link}
        >
          <Typography variant="h5">Gitlab Issue Tracker</Typography>
        </a>
        <a
          href="https://gitlab.com/cs-373-summer-2021/cs373-idb/-/tree/main"
          className={classes.link}
        >
          <Typography variant="h5">Gitlab Repo</Typography>
        </a>
        <a
          href="https://gitlab.com/cs-373-summer-2021/cs373-idb/-/wikis/home"
          className={classes.link}
        >
          <Typography variant="h5">Gitlab Wiki</Typography>
        </a>
        <a
          href="https://documenter.getpostman.com/view/16349010/Tzedf46U"
          className={classes.link}
        >
          <Typography variant="h5">Postman API</Typography>
        </a>
      </div>
      <div className={classes.main}>
        <a
          href="https://openlibrary.org/developers/api"
          className={classes.link}
        >
          <Typography variant="h5">OpenLibrary RESTful API</Typography>
        </a>
        <a
          href="https://www.mediawiki.org/wiki/API:Main_page"
          className={classes.link}
        >
          <Typography variant="h5">Wiki API</Typography>
        </a>
        <a
          href="https://speakerdeck.com/laylah/cs373-idb5-presentation"
          className={classes.link}
        >
          <Typography variant="h5">Speaker Deck</Typography>
        </a>
      </div>
    </div>
  );
};

export default About;
