/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import { api } from '../../services';
import Progress from '../../components/Progress';
import { AuthorCardProps } from '../../components/AuthorCard/types';
import SortingBar from '../../components/SortingBar';
import FilterMenu from '../../components/FilterMenu';
import SearchBar from '../../components/SearchBar';
import useStyles from './styles';
import AuthorCard from '../../components/AuthorCard';
import Pagination from '../../components/Pagination';

const Authors: React.FC = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [count, setCount] = useState(10);
  const [rowsPerPage, setRowsPerPage] = useState(15);
  const columns = ['Name (A-Z)', 'Birthday', 'Deathday', 'Number of Works'];
  // -- filter stuff --
  const filterOptionsByYear = ['Birthday', 'Deathday'];
  const filterOptionsByQuantity = ['Number of Works'];
  const filterOptionsByYesNo = ['Does this author have an active website?'];
  const [minDate, setMinDate] = useState('1900');
  const [maxDate, setMaxDate] = useState('2021');
  const [minDate2, setMinDate2] = useState('1900');
  const [maxDate2, setMaxDate2] = useState('2021');
  const [workMin, setWorkMin] = useState('0');
  const [workMax, setWorkMax] = useState('1000');
  const [letter, setLetter] = useState('');
  const [status, setStatus] = useState('active');
  const [country, setCountry] = useState('');
  const [filter, setFilter] = useState(false);
  // -- sorting stuff --
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'name';
    }
    if (givenOrderBy === columns[1]) {
      return 'birth_date';
    }
    if (givenOrderBy === columns[2]) {
      return 'death_date';
    }
    return 'num_works';
  };

  const [q, setQuery] = useState('');

  const getRange = (min: string, max: string) => {
    return `${min}-${max}`;
  };

  const getStatus = () => {
    return status === 'active';
  };

  useEffect(() => {
    const path = 'api/author/all/sorted';
    const params = filter
      ? {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
          birthday_range: getRange(minDate, maxDate),
          deathday_range: getRange(minDate2, maxDate2),
          works_range: getRange(workMin, workMax),
          has_website: getStatus(),
        }
      : {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
        };
    // console.log(params);
    api
      // .get(path, {params: { orderBy: getOrderBy(orderBy), ordering, q },
      .get(path, {
        params,
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })

      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, q, page, rowsPerPage, filter]);

  return (
    <div className={classes.root}>
      <h1>Authors</h1>
      <div className={classes.container}>
        <SortingBar
          columns={columns}
          setOrderBy={setOrderBy}
          options={options}
          setOrdering={setOrdering}
        />
        <div className={classes.filterMenu}>
          <FilterMenu
            filterOptionsByYear={filterOptionsByYear}
            filterOptionsByQuantity={filterOptionsByQuantity}
            filterOptionsByYesNo={filterOptionsByYesNo}
            filterOptionsByCountry={[]}
            filterOptionsBySubject={[]}
            filterOptionsByLetter={[]}
            setWorkMin={setWorkMin}
            setWorkMax={setWorkMax}
            setMinDate={setMinDate}
            setMaxDate={setMaxDate}
            setMinDate2={setMinDate2}
            setMaxDate2={setMaxDate2}
            setStatus={setStatus}
            setCountry={setCountry}
            filter={filter}
            setFilter={setFilter}
            setLetter={setLetter}
          />
        </div>
      </div>
      <div className={classes.space}>
        <div className={classes.search_root}>
          <SearchBar setQuery={setQuery} />
        </div>
      </div>
      {isLoading ? (
        <Progress />
      ) : (
        <>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="center"
            className={classes.gridContainer}
          >
            {data &&
              data
                // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((author: AuthorCardProps) => {
                  return (
                    <Grid
                      item
                      xs={4}
                      container
                      direction="row"
                      justify="center"
                      alignItems="center"
                    >
                      <AuthorCard
                        id={author.id}
                        name={author.name}
                        bio={author.bio}
                        top_work={author.top_work}
                        birth_date={author.birth_date}
                        death_date={author.death_date}
                        num_works={author.num_works}
                        website_link={author.website_link}
                        OLID={author.OLID}
                        books_list={author.books_list}
                        publisherID={author.publisherID}
                        searchQuery={q}
                      />
                    </Grid>
                  );
                })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Authors per page"
          />
        </>
      )}
    </div>
  );
};

export default Authors;
