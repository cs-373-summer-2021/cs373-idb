import React from 'react';
import Plot from 'react-plotly.js';
import { Typography } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
/* import {
  Chart,
  PieSeries,
  Title,
} from '@devexpress/dx-react-chart-material-ui';
import { Animation } from '@devexpress/dx-react-chart'; */

const totalNumBills = 232;

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      minHeight: '100vh',
      width: '100%',
      marginTop: theme.spacing(2),
    },
    media: {
      width: 300,
      height: 'auto',
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    gridContainer: {
      paddingLeft: theme.spacing(20),
      paddingRight: theme.spacing(20),
      paddingBottom: theme.spacing(3),
    },
    main: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(4),
    },
    link: {
      textDecoration: 'none',
    },
    input: {
      display: 'none',
    },
    inputRoot: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    centerContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    scrapingText: {
      width: '80%',
    },
    plot: {
      width: '100%',
      height: '100%',
    },
  });
});
const Politics: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h1>Politics In Review</h1>
      <Plot
        data={[
          {
            values: [
              17, 4, 5, 2, 2, 4, 38, 26, 4, 21, 11, 22, 2, 6, 13, 3, 11, 5, 11,
              2, 4, 1, 3, 4, 1, 3, 4, 1, 2,
            ],
            labels: [
              'Congress',
              'Native Americans',
              'Economics and Public Finance',
              'Environmental Protection',
              'Energy',
              'Armed Forces and National Security',
              'Health',
              'Crime and Law Enforcement',
              'Civil Rights and Liberties, Minority Issues',
              'International Affairs',
              'Government Operations and Politics',
              'Taxation',
              'Families',
              'Science, Technology, Communications',
              'Labor and Employment',
              'Emergency Management',
              'Education',
              'Public Lands and Natural Resources',
              'Transportation and Public Works',
              'Social Welfare',
              'Commerce',
              'Law',
              'Housing and Community Development',
              'Finance and Financial Sector',
              'Immigration',
              'Foreign Trade and International Finance',
              'Arts, Culture, Religion',
              'Agriculture and Food',
              'Water Resources Development',
            ],
            type: 'pie',
          },
        ]}
        layout={{
          autosize: true,
          title: 'Subjects Frequency in Bills',
        }}
        className={classes.plot}
        useResizeHandler
      />
      <Plot
        data={[
          {
            x: [
              'WI',
              'WY',
              'CO',
              'TN',
              'CT',
              'MO',
              'NJ',
              'AR',
              'OH',
              'NC',
              'WA',
              'MD',
              'DE',
              'PA',
              'ME',
              'TX',
              'ND',
              'ID',
              'MT',
              'IL',
              'CA',
              'NE',
              'NY',
              'SC',
              'IA',
              'NM',
              'HI',
              'OK',
              'AZ',
              'MN',
              'VT',
              'UT',
              'WV',
              'MA',
              'OR',
              'KS',
              'AK',
              'KY',
              'MI',
              'RI',
              'FL',
              'NH',
              'AL',
              'SD',
              'VA',
              'MS',
              'IN',
              'NV',
              'GA',
              'DC',
              'LA',
              'MP',
            ],
            y: [
              4, 0, 3, 2, 6, 1, 5, 0, 4, 2, 4, 6, 2, 2, 1, 8, 0, 0, 1, 9, 29, 0,
              9, 1, 0, 2, 2, 0, 4, 2, 2, 0, 1, 6, 6, 0, 0, 1, 3, 4, 8, 2, 1, 0,
              3, 1, 1, 1, 3, 1, 1, 1,
            ],
            name: 'Democrats',
            type: 'bar',
            marker: {
              color: 'blue',
            },
          },
          {
            x: [
              'WI',
              'WY',
              'CO',
              'TN',
              'CT',
              'MO',
              'NJ',
              'AR',
              'OH',
              'NC',
              'WA',
              'MD',
              'DE',
              'PA',
              'ME',
              'TX',
              'ND',
              'ID',
              'MT',
              'IL',
              'CA',
              'NE',
              'NY',
              'SC',
              'IA',
              'NM',
              'HI',
              'OK',
              'AZ',
              'MN',
              'VT',
              'UT',
              'WV',
              'MA',
              'OR',
              'KS',
              'AK',
              'KY',
              'MI',
              'RI',
              'FL',
              'NH',
              'AL',
              'SD',
              'VA',
              'MS',
              'IN',
              'NV',
              'GA',
              'DC',
              'LA',
              'MP',
            ],
            y: [
              1, 2, 1, 3, 0, 5, 1, 4, 10, 4, 2, 1, 0, 4, 2, 10, 2, 3, 1, 2, 7,
              3, 1, 5, 1, 0, 0, 4, 2, 1, 1, 2, 1, 0, 0, 1, 2, 5, 4, 0, 7, 0, 4,
              1, 2, 2, 3, 1, 1, 0, 1, 0,
            ],
            name: 'Republicans',
            type: 'bar',
            marker: {
              color: 'red',
            },
          },
        ]}
        layout={{
          barmode: 'group',
          autosize: true,
          title: 'Political Party Members by State',
        }}
        className={classes.plot}
        useResizeHandler
      />
    </div>
  );
};

export default Politics;
