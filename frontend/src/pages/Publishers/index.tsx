import React, { useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import { PublisherCardProps } from '../../components/PublisherCard/types';
import useStyles from './styles';
import SortingBar from '../../components/SortingBar';
import PublisherCard from '../../components/PublisherCard';
import { api } from '../../services';
import Progress from '../../components/Progress';
import FilterMenu from '../../components/FilterMenu';
import SearchBar from '../../components/SearchBar';
import Pagination from '../../components/Pagination';

const Publishers: React.FC = () => {
  const classes = useStyles();
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(15);
  const [count, setCount] = useState(10);
  const [isLoading, setIsLoading] = useState(true);
  const columns = [
    'Name (A-Z)',
    'Status',
    'Date Founded',
    'Country',
    'Headquarters',
  ];
  // -- filter stuff --
  const filterOptionsByYear = ['Year Founded'];
  const filterOptionsByCountry = ['Home Country'];
  const filterOptionsByYesNo = ['Is this publisher active?'];
  const [minDate, setMinDate] = useState('1900');
  const [maxDate, setMaxDate] = useState('2021');
  const [minDate2, setMinDate2] = useState('1900');
  const [maxDate2, setMaxDate2] = useState('2021');
  const [workMin, setWorkMin] = useState('0');
  const [workMax, setWorkMax] = useState('1000');
  const [letter, setLetter] = useState('');
  const [status, setStatus] = useState('active');
  const [country, setCountry] = useState('');
  const [filter, setFilter] = useState(false);
  // -- sorting stuff --
  const [orderBy, setOrderBy] = useState(columns[0]);
  const options = ['asc', 'desc'];
  const [ordering, setOrdering] = useState('asc'); // asc or desc

  const getOrderBy = (givenOrderBy: string) => {
    if (givenOrderBy === columns[0]) {
      return 'name';
    }
    if (givenOrderBy === columns[1]) {
      return 'status';
    }
    if (givenOrderBy === columns[2]) {
      return 'founded';
    }
    if (givenOrderBy === columns[3]) {
      return 'country';
    }
    return 'headquarters';
  };

  const getRange = () => {
    return `${minDate}-${maxDate}`;
  };

  const [data, setData] = useState([]);
  const [q, setQuery] = useState('');

  useEffect(() => {
    const path = 'api/publisher/all/sorted';
    const params = filter
      ? {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
          founded_range: getRange(),
          country,
          status,
        }
      : {
          orderBy: getOrderBy(orderBy),
          ordering,
          q,
          page,
          per_page: rowsPerPage,
        };
    // console.log(params);
    api
      .get(path, {
        params,
      })
      .then(value => {
        setIsLoading(false);
        setData(value.data.results);
        setCount(value.data.count);
      })
      .catch(err => {
        console.log(err);
      });
  }, [orderBy, ordering, q, page, rowsPerPage, filter]);

  return (
    <div className={classes.root}>
      <h1>Publishers</h1>
      <div className={classes.container}>
        <SortingBar
          columns={columns}
          setOrderBy={setOrderBy}
          options={options}
          setOrdering={setOrdering}
        />
        <div className={classes.filterMenu}>
          <FilterMenu
            filterOptionsByYear={filterOptionsByYear}
            filterOptionsByQuantity={[]}
            filterOptionsByYesNo={filterOptionsByYesNo}
            filterOptionsByCountry={filterOptionsByCountry}
            filterOptionsBySubject={[]}
            filterOptionsByLetter={[]}
            setWorkMin={setWorkMin}
            setWorkMax={setWorkMin}
            setMinDate={setMinDate}
            setMaxDate={setMaxDate}
            setMinDate2={setMinDate2}
            setMaxDate2={setMaxDate2}
            setStatus={setStatus}
            setCountry={setCountry}
            filter={filter}
            setFilter={setFilter}
            setLetter={setLetter}
          />
        </div>
      </div>
      <div className={classes.space}>
        <div className={classes.search_root}>
          <SearchBar setQuery={setQuery} />
        </div>
      </div>
      {isLoading ? (
        <Progress />
      ) : (
        <>
          <Grid
            container
            direction="row"
            justify="space-evenly"
            spacing={3}
            alignItems="center"
            className={classes.gridContainer}
          >
            {data &&
              data
                // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((publisher: PublisherCardProps) => {
                  return (
                    <Grid
                      item
                      xs={4}
                      container
                      direction="row"
                      justify="center"
                      alignItems="center"
                    >
                      <PublisherCard
                        id={publisher.id}
                        name={publisher.name}
                        status={publisher.status}
                        founded={publisher.founded}
                        country={publisher.country}
                        headquarters={publisher.headquarters}
                        website={publisher.website}
                        books={publisher.books}
                        authors={publisher.authors}
                        image_link={publisher.image_link}
                        searchQuery={q}
                      />
                    </Grid>
                  );
                })}
          </Grid>
          <Pagination
            count={count}
            page={page}
            setPage={setPage}
            rowsPerPage={rowsPerPage}
            setRowsPerPage={setRowsPerPage}
            labelRowsPerPage="Publishers per page"
          />
        </>
      )}
    </div>
  );
};

export default Publishers;
