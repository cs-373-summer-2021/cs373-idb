import axios from 'axios';

export const api = axios.create({
  // baseURL: 'https://cs373-idb-317819.uc.r.appspot.com/',
  baseURL: 'http://localhost:5000/',
});

export const apiOLID = axios.create({
  baseURL: 'http://covers.openlibrary.org/',
});

export const otherAPI = axios.create({
  baseURL: 'https://api.politicsin.review/',
});
