import React from 'react';
import { Route, Switch } from 'react-router-dom';
import About from '../pages/About';
import Authors from '../pages/Authors';

import Homepage from '../pages/Homepage';
import AuthorInfoPage from '../pages/InfoPages/AuthorInfo';
import PublisherInfoPage from '../pages/InfoPages/PublisherInfo';
import WorkInfoPage from '../pages/InfoPages/WorkInfo';
import Publishers from '../pages/Publishers';
import Works from '../pages/Works';
import GlobalSearch from '../pages/GlobalSearch';
import Politics from '../pages/Politics';

const Routes: React.FC = () => {
  return (
    <Switch>
      <Route component={Homepage} exact path="/" />
      <Route component={About} exact path="/about" />
      <Route component={Authors} exact path="/authors" />
      <Route
        path="/authors/:id"
        render={({
          match: {
            params: { id },
          },
        }) => <AuthorInfoPage id={parseInt(id, 10)} />}
      />
      <Route component={Publishers} exact path="/publishers" />
      <Route
        path="/publishers/:id"
        render={({
          match: {
            params: { id },
          },
        }) => <PublisherInfoPage id={parseInt(id, 10)} />}
      />
      <Route component={Works} exact path="/works" />
      <Route
        path="/works/:id"
        render={({
          match: {
            params: { id },
          },
        }) => <WorkInfoPage id={parseInt(id, 10)} />}
      />
      <Route
        path="/search/:q"
        render={({
          match: {
            params: { q },
          },
        }) => <GlobalSearch searchQuery={q} />}
      />
      <Route component={GlobalSearch} exact path="/search" />
      <Route component={Politics} exact path="/politics" />
    </Switch>
  );
};

export default Routes;
