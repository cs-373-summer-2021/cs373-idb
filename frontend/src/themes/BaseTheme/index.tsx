import { CSSProperties } from 'react';
import { createMuiTheme } from '@material-ui/core/styles';

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    customPalette: {
      common: {
        lightPurple?: CSSProperties['color'];
        tan?: CSSProperties['color'];
        gray?: CSSProperties['color'];
        green?: CSSProperties['color'];
        greenBlue?: CSSProperties['color'];
        lightBlue?: CSSProperties['color'];
      };
    };
    customTypography: {
      tab: {
        textTransform: CSSProperties['textTransform'];
        fontFamily: CSSProperties['fontFamily'];
        fontWeight: CSSProperties['fontWeight'];
        fontSize: CSSProperties['fontSize'];
      };
    };
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    customPalette?: {
      common?: {
        lightPurple?: CSSProperties['color'];
        tan?: CSSProperties['color'];
        gray?: CSSProperties['color'];
        green?: CSSProperties['color'];
        greenBlue?: CSSProperties['color'];
        lightBlue?: CSSProperties['color'];
      };
    };
    customTypography?: {
      tab?: {
        textTransform?: CSSProperties['textTransform'];
        fontFamily?: CSSProperties['fontFamily'];
        fontWeight?: CSSProperties['fontWeight'];
        fontSize?: CSSProperties['fontSize'];
      };
    };
  }
}

const myLightPurple = '#D9DBF1';
const myTan = '#D0CDD7';
const myGray = '#ACB0BD';
const myGreen = '#416165';
const myGreenBlue = '#0B3948';
const myLightBlue = '#61dbfb';

const BaseThemeProvider = createMuiTheme({
  palette: {
    primary: {
      main: myGray,
    },
    secondary: {
      main: myGreen,
    },
  },
  typography: {
    h2: {
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 500,
      fontSize: '2.5rem',
      lineHeight: 1.5,
    },
  },
  customPalette: {
    common: {
      lightPurple: myLightPurple,
      tan: myTan,
      gray: myGray,
      green: myGreen,
      greenBlue: myGreenBlue,
      lightBlue: myLightBlue,
    },
  },
  customTypography: {
    tab: {
      textTransform: 'none',
      fontFamily: 'Montserrat, sans-serif',
      fontWeight: 500,
      fontSize: '1rem',
    },
  },
});

export default BaseThemeProvider;
