import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from '@material-ui/core';

import BaseTheme from './BaseTheme';

const BaseThemeProvider: React.FC = ({ children }) => {
  return <ThemeProvider theme={BaseTheme}>{children}</ThemeProvider>;
};

BaseThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BaseThemeProvider;
