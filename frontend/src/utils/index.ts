const getFirstThree = (array: number[]) => {
  const result: number[] = [];
  let i = 0;
  while (i < 3) {
    if (i < array.length) {
      result.push(array[i]);
    }
    i += 1;
  }
  return result;
};

export default getFirstThree;
