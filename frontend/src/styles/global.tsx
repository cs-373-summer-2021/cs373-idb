import React from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import { StylesProvider } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const GlobalStyleProvider: React.FC = ({ children }) => {
  return (
    <StylesProvider injectFirst>
      <CssBaseline />
      {children}
    </StylesProvider>
  );
};

GlobalStyleProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GlobalStyleProvider;
