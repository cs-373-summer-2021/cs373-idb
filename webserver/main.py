from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__, static_folder='build', static_url_path="/")
cors = CORS(app)

@app.route("/heartbeat")
def heartbeat():
    return jsonify({"status": "healthy"})


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return app.send_static_file("index.html")


@app.errorhandler(404)
def page_not_found(e):
    # reroute to home
    return app.send_static_file('index.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')