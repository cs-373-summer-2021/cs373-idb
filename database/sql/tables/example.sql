DROP TABLE IF EXISTS public.example;

CREATE TABLE public.example (
	id bigserial NOT null,

	my_name text,
	
	my_float float(53) not NULL,

	is_on boolean not null,

	CONSTRAINT configs_pk PRIMARY KEY (id)
);
